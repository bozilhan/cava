package com.dailycodebuffer.cloudgateway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallBackMethodController {
    @GetMapping(path = "{userServiceFallBack}")
    public String userServiceFallBackMethod() {
        return "User service is taking longer than expected. Please try again later";
    }

    @GetMapping(path = "{departmentServiceFallBack}")
    public String departmentServiceFallBackMethod() {
        return "Department service is taking longer than expected. Please try again later";
    }
}
