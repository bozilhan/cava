package com.dailycodebuffer.department.controller;

import com.dailycodebuffer.department.entity.Department;
import com.dailycodebuffer.department.exception.ApiRequestException;
import com.dailycodebuffer.department.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/departments")
@Slf4j
public class DepartmentController {
    private final DepartmentService departmentService;

    @Autowired
    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @PostMapping()
    public Department saveDepartment(@NonNull @RequestBody Department department) {
        log.info("DepartmentController::saveDepartment - department", department);
        return departmentService.saveDepartment(department);
    }

    @GetMapping(path = "{departmentId}")
    public Department findDepartmentById(@PathVariable("departmentId") String departmentId) {
        log.info("DepartmentController::findDepartmentById");
        return departmentService.findDepartmentById(departmentId)
                .orElseThrow(() -> new ApiRequestException(""));
    }
}
