package com.dailycodebuffer.department.service;

import com.dailycodebuffer.department.entity.Department;
import com.dailycodebuffer.department.repository.DepartmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class DepartmentService {
    private final DepartmentRepository departmentRepository;

    @Autowired
    public DepartmentService(@Qualifier("departmentMongo") DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    public Department saveDepartment(Department department) {
        log.info("DepartmentService::saveDepartment - department", department);
        return departmentRepository.save(department);
    }

    public Optional<Department> findDepartmentById(String departmentId) {
        log.info("DepartmentService::findDepartmentById");
        return departmentRepository.findById(departmentId);
    }
}
