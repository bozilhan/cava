package com.dailycodebuffer.user.controller;

import com.dailycodebuffer.user.entity.User;
import com.dailycodebuffer.user.service.UserService;
import com.dailycodebuffer.user.valueobjects.ResponseTemplateValueObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@Slf4j
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public User saveUser(@RequestBody User user) {
        log.info("UserController::saveUser - user", user);
        return userService.saveUser(user);
    }

    /*
     * get the user along with the department which he belongs to
     *
     * we need to create the wrapper object
     * that will contain user object and department object
     * */
    @GetMapping(path = "{userId}")
    public ResponseTemplateValueObject getUserWithDepartment(@PathVariable("userId") Long userId) {
        log.info("UserController::getUserWithDepartment");
        return userService.getUserWithDepartment(userId);
    }
}
