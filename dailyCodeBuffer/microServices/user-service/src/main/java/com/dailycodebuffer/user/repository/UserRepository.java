package com.dailycodebuffer.user.repository;

import com.dailycodebuffer.user.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("postgresRepository")
public interface UserRepository extends CrudRepository<User, Long> {
}
