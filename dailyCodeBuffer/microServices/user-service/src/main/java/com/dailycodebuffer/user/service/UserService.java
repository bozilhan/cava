package com.dailycodebuffer.user.service;

import com.dailycodebuffer.user.entity.User;
import com.dailycodebuffer.user.exception.ApiRequestException;
import com.dailycodebuffer.user.repository.UserRepository;
import com.dailycodebuffer.user.valueobjects.Department;
import com.dailycodebuffer.user.valueobjects.ResponseTemplateValueObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class UserService {
    private final UserRepository userRepository;

    /*
     * with this RestTemplate we will call the department service and will get the Department object
     * */
    private RestTemplate restTemplate;

    @Autowired
    public UserService(@Qualifier("postgresRepository") UserRepository userRepository, RestTemplate restTemplate) {
        this.userRepository = userRepository;
        this.restTemplate = restTemplate;
    }

    public User saveUser(User user) {
        log.info("UserService::saveUser - user", user);
        return userRepository.save(user);
    }

    public ResponseTemplateValueObject getUserWithDepartment(Long userId) {
        log.info("UserService::getUserWithDepartment - entry");
        ResponseTemplateValueObject responseTemplateValueObject = new ResponseTemplateValueObject();
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ApiRequestException("UserService::getUserWithDepartment - user is not hound"));

        /*
         * we need to get the department for that particular user
         *
         * to get department object there is another microservice
         * we need to call the department microservice and
         * get the department object from there
         *
         * we need to RestTemplate object
         * */
        Department department = restTemplate
                .getForObject("http://DEPARTMENT-SERVICE/departments" + user.getDepartmentId(), Department.class);

        responseTemplateValueObject.setUser(user);
        responseTemplateValueObject.setDepartment(department);

        return responseTemplateValueObject;
    }
}
