package com.dailycodebuffer.user.valueobjects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/*
 * we don't have to create entity
 * we just need a simple pojo
 * */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Department {
    private String departmentId;
    private String departmentName;
    private String departmentAddress;
    private String departmentCode;
}

