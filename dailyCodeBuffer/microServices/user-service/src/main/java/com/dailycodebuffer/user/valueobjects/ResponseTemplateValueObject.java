package com.dailycodebuffer.user.valueobjects;

import com.dailycodebuffer.user.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/*
 * wrapper object that will contain both User and Department
 *
 * we will use ResponseTemplateValueObject as our return type
 * so we can return our user and as well as its department
 * */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ResponseTemplateValueObject {
    private User user;
    private Department department;
}
