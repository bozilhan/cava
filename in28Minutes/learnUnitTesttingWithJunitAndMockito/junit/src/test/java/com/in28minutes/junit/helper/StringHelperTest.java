package com.in28minutes.junit.helper;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

// JUNIT4
//  this is a parametrized is NOT A NORMAL test.
// I will not run this test in a normal way.
// I run with a few parameters.
//@RunWith(Parameterized.class)
class StringHelperTest {
    private StringHelper stringHelper = new StringHelper();

    @Test
    public void testTruncateAInFirst2Positions_AinFirst2Positions() {
        assertEquals("CD", this.stringHelper.truncateAInFirst2Positions("AACD"));
    }

    @Test
    public void testTruncateAInFirst2Positions_AinFirstPosition() {
        assertEquals("CD", this.stringHelper.truncateAInFirst2Positions("ACD"));
    }

//    private String input;
//    private String expectedOutput;
//
//    public StringHelperParametrizedTest(String input, String expectedOutput) {
//        this.input = input;
//        this.expectedOutput = expectedOutput;
//    }
//
//    // The second thing that I would need to define is the arguments that we would need to pass.
//    @Parameters
//    public static Collection<String[]> testConditions() {
//        // First let's create an 2D array with the expected input and the output
//        // {EXPECTED_INPUT, EXPECTED_OUTPUT}
//        String expectedOutputs[][] = {{"AACD", "CD"},
//                {"ACD", "CD"}};
//
//        return Arrays.asList(expectedOutputs);
//    }
//
//    @Test
//    public void testTruncateAInFirst2Positions_AinFirst2Positions() {
//        assertEquals(expectedOutput, this.stringHelper.truncateAInFirst2Positions(input));
//    }


}