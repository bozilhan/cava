package com.in28minutes.business;

import com.in28minutes.data.api.TodoService;

import java.util.ArrayList;
import java.util.List;

// SYSTEM UNDER TEST SUT
// TodoBusinessImp is SUT
// TodoService is dependency Dependency
public class TodoBusinessImpl {
    // I would want to make use of that TodoService
    private TodoService todoService;

    // constructor injection
    public TodoBusinessImpl(TodoService todoService) {
        this.todoService = todoService;
    }

    // This is the method we want to write tests
    public List<String> retrieveTodosRelatedToSpring(String user) {
        List<String> filteredTodos = new ArrayList<String>();
        List<String> todos = this.todoService.retrieveTodos(user);

        // write logic now to filter the todos and only return a list of todos containing spring
        // I would want to filter those which are related to spring.
        for (String todo : todos) {
            if (todo.contains("Spring")) {
                filteredTodos.add(todo);
            }
        }
        return filteredTodos;
    }

    // This is the method we want to write tests
    // this method does NOT RETURN A VALUE to ASSERT AGAINST
    // return type is VOID
    public void deleteTodosNotRelatedToSpring(String user) {
        List<String> todos = this.todoService.retrieveTodos(user);

        // Spring kelimesi iceren bir eleman icin todoService.deleteTodo CAGIRILMAZ!!!
        for (String todo : todos) {
            if (!todo.contains("Spring")) {
                // deleting all todos
                // NOT containing a substring "Spring"
                todoService.deleteTodo(todo);
            }
        }
    }
}
