package com.in28minutes.busines;

import com.in28minutes.business.TodoBusinessImpl;
import com.in28minutes.data.api.TodoService;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ListTest {
    @Test
    public void letsMockListSizeMethod() {
        // mock the list interface
        List listMock = mock(List.class);

        when(listMock.size()).thenReturn(2);

        assertEquals(2, listMock.size());
        assertEquals(2, listMock.size());
        assertEquals(2, listMock.size());
    }

    @Test
    public void letsMockListSize_ReturnMultipleValues() {
        // mock the list interface
        List listMock = mock(List.class);

        // the first time that listMock.size() it returns 2
        // the second time that listMock.size() it returns 3
        when(listMock.size()).thenReturn(2).thenReturn(3);

        assertEquals(2, listMock.size());
        assertEquals(3, listMock.size());
    }

    @Test
    public void letsMockListGet() {
        // mock the list interface
        List listMock = mock(List.class);

        // the first time that listMock.size() it returns 2
        // the second time that listMock.size() it returns 3
        when(listMock.get(0)).thenReturn("in28Minutes");

        assertEquals("in28Minutes", listMock.get(0));
        // When you DONT TELL anything to mocks, mocks return DEFAULTS
        assertNull(null, listMock.get(1));
    }

    @Test
    public void letsMockListGet_usingBDD() {
        // GIVEN Setup part
        // mock the list interface
        List<String> listMock = mock(List.class);

        // the first time that listMock.size() it returns 2
        // the second time that listMock.size() it returns 3
        given(listMock.get(anyInt())).willReturn("in28Minutes");

        // WHEN actual system invocation
        String firstElement = listMock.get(0);

        // THEN asserts
        assertThat(firstElement, is("in28Minutes"));
    }

    @Test
    public void letsMockListGet_anyIntArgumentMatcher() {
        List listMock = mock(List.class);

        // ARGUMENT MATCHER due to anyInt(), anyDouble(), anyWarning()
        when(listMock.get(anyInt())).thenReturn("in28Minutes");

        assertEquals("in28Minutes", listMock.get(0));
        assertEquals("in28Minutes", listMock.get(1));
        assertEquals("in28Minutes", listMock.get(2));
    }

    @Test(expected = RuntimeException.class)
    public void letsMockListGet_throwException() {
        List listMock = mock(List.class);

        // ARGUMENT MATCHER due to anyInt(), anyDouble(), anyWarning()
        when(listMock.get(anyInt())).thenThrow(new RuntimeException("Something Runtime exception"));

        listMock.get(0);
    }
}
