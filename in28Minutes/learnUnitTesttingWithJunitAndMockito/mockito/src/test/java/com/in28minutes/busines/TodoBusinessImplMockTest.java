package com.in28minutes.busines;

import com.in28minutes.business.TodoBusinessImpl;
import com.in28minutes.data.api.TodoService;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

public class TodoBusinessImplMockTest {
    // WHAT is MOCKING
    // mocking is creating objects that simulate the behaviours of real objects
    // UNLIKE STUBS, mocks can be DYNAMICALLY CREATED from code - AT RUNTIME
    // Mocks offer MORE FUNCTİONALİTY than stubbing
    // You can verify method calls and a lot of other settings
    @Test
    public void testRetrieveTodosRelatedToSpring_usingMock() {
        // mock() method is defined in Mockito class
        // you can mock a class or interface
        // 1.
        TodoService todoServiceMock = mock(TodoService.class);

        // 2.
        List<String> todos = Arrays.asList("Learn Spring MVC", "Learn Spring", "Learn to Dance");

        // 3.
        // we made the mock when it's CALLED with a SPECIFIC METHOD to RETURN a SPECIFIC VALUE.
        // So when the toDoServiceMock is called with a dummy value it has to return these todos list back.
        // So in these three lines of code we have created something very similar to this stub that we had created earlier.
        // when() is to use to stub a method
        // thenReturn() SETS a return value to be returned when the method is called.
        // when this method is called with a specific value then return this thing back.
        when(todoServiceMock.retrieveTodos("Dummy")).thenReturn(todos);

        TodoBusinessImpl todoBusinessImpl = new TodoBusinessImpl(todoServiceMock);

        List<String> filteredTodos = todoBusinessImpl.retrieveTodosRelatedToSpring("Dummy");

        assertEquals(2, filteredTodos.size());
    }

    @Test
    public void testRetrieveTodosRelatedToSpring_usingBDD() {
        // Given GIVEN should be everything that is doing the set up.
        // When in gerceklestirilmesi icin gereken ON KOSULLAR
        TodoService todoServiceMock = mock(TodoService.class);

        List<String> todos = Arrays.asList("Learn Spring MVC", "Learn Spring", "Learn to Dance");

        // we think GIVEN this particular thing this will return todos
        given(todoServiceMock.retrieveTodos("Dummy")).willReturn(todos);

        TodoBusinessImpl todoBusinessImpl = new TodoBusinessImpl(todoServiceMock);

        // When is the business method action.
        // When is the specific action that needs to be done.
        // This is the method call which is doing the actual event
        // when this happens
        List<String> filteredTodos = todoBusinessImpl.retrieveTodosRelatedToSpring("Dummy");

        // Then
        // So that filteredTodos.size() is two.
        assertThat(filteredTodos.size(), is(2));

        // we think GIVEN this particular thing this will return todos
        // when this happens
        // So that filteredTodos.size() is two.
    }

    @Test
    public void testDeleteTodosNotRelatedToSpring_usingBDD() {
        // Given GIVEN should be everything that is doing the set up.
        // When in gerceklestirilmesi icin gereken ON KOSULLAR
        TodoService todoServiceMock = mock(TodoService.class);

        List<String> todos = Arrays.asList("Learn Spring MVC", "Learn Spring", "Learn to Dance");

        // we think GIVEN this particular thing this will return todos
        given(todoServiceMock.retrieveTodos("Dummy")).willReturn(todos);

        TodoBusinessImpl todoBusinessImpl = new TodoBusinessImpl(todoServiceMock);

        // When is the business method action.
        // When is the specific action that needs to be done.
        // This is the method call which is doing the actual event
        // when this happens
        todoBusinessImpl.deleteTodosNotRelatedToSpring("Dummy");

        // Then
        verify(todoServiceMock).deleteTodo("Learn to Dance");
        then(todoServiceMock).should().deleteTodo("Learn to Dance");
        then(todoServiceMock).should(never()).deleteTodo("Learn Spring MVC");
        verify(todoServiceMock, never()).deleteTodo("Learn Spring MVC");
        verify(todoServiceMock, never()).deleteTodo("Learn Spring");
    }

    @Test
    public void testDeleteTodosNotRelatedToSpring_usingBDDThenInsteadOfVerify() {
        // Given GIVEN should be everything that is doing the set up.
        // When in gerceklestirilmesi icin gereken ON KOSULLAR
        TodoService todoServiceMock = mock(TodoService.class);

        List<String> todos = Arrays.asList("Learn Spring MVC", "Learn Spring", "Learn to Dance");

        // we think GIVEN this particular thing this will return todos
        given(todoServiceMock.retrieveTodos("Dummy")).willReturn(todos);

        TodoBusinessImpl todoBusinessImpl = new TodoBusinessImpl(todoServiceMock);

        // When is the business method action.
        // When is the specific action that needs to be done.
        // This is the method call which is doing the actual event
        // when this happens
        todoBusinessImpl.deleteTodosNotRelatedToSpring("Dummy");

        // Then
        then(todoServiceMock).should().deleteTodo("Learn to Dance");
        then(todoServiceMock).should(never()).deleteTodo("Learn Spring MVC");
        then(todoServiceMock).should(never()).deleteTodo("Learn Spring");
    }

    @Test
    public void testDeleteTodosNotRelatedToSpring_usingBDD_argumentCapture() {
        // DECLARE AN ARGUMENT CAPTOR
        ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);

        // Given GIVEN should be everything that is doing the set up.
        // When in gerceklestirilmesi icin gereken ON KOSULLAR
        TodoService todoServiceMock = mock(TodoService.class);

        List<String> todos = Arrays.asList("Learn Spring MVC", "Learn Spring", "Learn to Dance");

        // we think GIVEN this particular thing this will return todos
        given(todoServiceMock.retrieveTodos("Dummy")).willReturn(todos);

        TodoBusinessImpl todoBusinessImpl = new TodoBusinessImpl(todoServiceMock);

        // When is the business method action.
        // When is the specific action that needs to be done.
        // This is the method call which is doing the actual event
        // when this happens
        todoBusinessImpl.deleteTodosNotRelatedToSpring("Dummy");

        // Then
        // I want to see what was the argument which was passed to deleteTodo()
        // DEFINE ARGUMENT CAPTOR ON SPECIFIC METHOD CALL
        then(todoServiceMock).should().deleteTodo(stringArgumentCaptor.capture());

        // CAPTURE THE ARGUMENT CHECK IT
        assertThat(stringArgumentCaptor.getValue(), is("Learn to Dance"));
    }

    @Test
    public void testDeleteTodosNotRelatedToSpring_usingBDD_argumentCaptureMultipleTImes() {
        // DECLARE AN ARGUMENT CAPTOR
        ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);

        // Given GIVEN should be everything that is doing the set up.
        // When in gerceklestirilmesi icin gereken ON KOSULLAR
        TodoService todoServiceMock = mock(TodoService.class);

        List<String> todos = Arrays.asList("Learn to Rock and Roll", "Learn Spring", "Learn to Dance");

        // we think GIVEN this particular thing this will return todos
        given(todoServiceMock.retrieveTodos("Dummy")).willReturn(todos);

        TodoBusinessImpl todoBusinessImpl = new TodoBusinessImpl(todoServiceMock);

        // When is the business method action.
        // When is the specific action that needs to be done.
        // This is the method call which is doing the actual event
        // when this happens
        todoBusinessImpl.deleteTodosNotRelatedToSpring("Dummy");

        // Then
        // I want to see what was the argument which was passed to deleteTodo()
        // DEFINE ARGUMENT CAPTOR ON SPECIFIC METHOD CALL
        then(todoServiceMock).should(times(2)).deleteTodo(stringArgumentCaptor.capture());

        // CAPTURE THE ARGUMENT CHECK IT
        // You might want to check more than just the size
        assertThat(stringArgumentCaptor.getAllValues().size(), is(2));
    }

    @Test
    public void testRetrieveTodosRelatedToSpring_usingMockWithEmptyList() {
        // mock() method is defined in Mockito class
        // you can mock a class or interface
        // 1.
        TodoService todoServiceMock = mock(TodoService.class);

        // 2.
        List<String> todos = Collections.emptyList();

        // 3.
        // we made the mock when it's CALLED with a SPECIFIC METHOD to RETURN a SPECIFIC VALUE.
        // So when the toDoServiceMock is called with a dummy value it has to return these todos list back.
        // So in these three lines of code we have created something very similar to this stub that we had created earlier.
        // when() is to use to stub a method
        // thenReturn() SETS a return value to be returned when the method is called.
        // when this method is called with a specific value then return this thing back.
        when(todoServiceMock.retrieveTodos("Dummy")).thenReturn(todos);

        TodoBusinessImpl todoBusinessImpl = new TodoBusinessImpl(todoServiceMock);

        List<String> filteredTodos = todoBusinessImpl.retrieveTodosRelatedToSpring("Dummy");
        assertEquals(0, filteredTodos.size());
    }
}
