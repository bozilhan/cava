package com.in28minutes.data.api;

import java.util.Arrays;
import java.util.List;

// DUMMY IMPLEMENTATION of TodoService
public class TodoServiceStub implements TodoService {
    // DISADVANTAGES
    // Dynamic Condition
    // Service Definition
    public List<String> retrieveTodos(String user) {
        return Arrays.asList("Learn Spring MVC", "Learn Spring", "Learn to Dance");
    }

    public void deleteTodo(String todo) {

    }
}
