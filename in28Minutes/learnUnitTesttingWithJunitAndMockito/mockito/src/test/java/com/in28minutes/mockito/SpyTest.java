package com.in28minutes.mockito;

import org.junit.Test;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class SpyTest {
    @Test
    public void test1() {
        // A SPY gets all logic from the class
        // spy(ArrayList.class) CREATING REAL ArrayList
        // You can stub specific methods of your choice
        // A spy by default retains all logic from ArrayList
        List arrayListSpy = spy(ArrayList.class);

        assertEquals(0, arrayListSpy.size());

        arrayListSpy.add("Dummy");
        assertEquals(1, arrayListSpy.size());

        arrayListSpy.remove("Dummy");
        assertEquals(0, arrayListSpy.size());
    }

    @Test
    public void test2() {
        List arrayListSpy = spy(ArrayList.class);

        // the GREAT thing about the SPY is that you can OVERRIDE SPECIFIC METHOD.
        // Spy is also called a PARTIAL MOCK
        // the other methods in ArrayList will remain the default functionality.
        // HOWEVER only the size() method I'm all overriding it. I'm stabbing it to return a value 5.
        // So this is almost like a partial implementation of the ArrayList.
        // All the other functionality we'll still continue working on the ArrayList as NORMAL.
        // So spies allow you to keep track of what's happening with the real object
        // as well as they allow you to override a specific behavior.
        // AVOID SPY in Projects
        stub(arrayListSpy.size()).toReturn(5);

        assertEquals(5, arrayListSpy.size());
    }

    @Test
    public void test3() {
        List arrayListSpy = spy(ArrayList.class);

        // the GREAT thing about the SPY is that you can OVERRIDE SPECIFIC METHOD.
        // Spy is also called a PARTIAL MOCK
        // the other methods in ArrayList will remain the default functionality.
        // HOWEVER only the size() method I'm all overriding it. I'm stabbing it to return a value 5.
        // So this is almost like a partial implementation of the ArrayList.
        // All the other functionality we'll still continue working on the ArrayList as NORMAL.
        arrayListSpy.add("Dummy");
        verify(arrayListSpy).add("Dummy");
        verify(arrayListSpy, never()).clear();
    }
}
