package com.in28minutes.powermock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SystemUnderTest.class)
// Step1. PrepareForTest => SystemUnderTest.class
public class MockingConstructorTest {
    @InjectMocks
    SystemUnderTest systemUnderTest;

    @Mock
    ArrayList mockList ;

    @Test
    public void testRetrieveTodosRelatedToSpring_usingMock() throws Exception {
        when(mockList.size()).thenReturn(5);

        // I want to mock the constructor of arrayList
        // Step2. Override the constructor
        PowerMockito.whenNew(ArrayList.class).withAnyArguments().thenReturn(mockList);

        int size = systemUnderTest.methodUsingAnArrayListConstructor();

        assertEquals(5, size);
    }
}
