package com.in28minutes.powermock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

// when you want to combine Mockito and  powermock
// you have to use a SPECIFIC RUNNER class.
@RunWith(PowerMockRunner.class)
@PrepareForTest(UtilityClass.class)
// @PrepareForTest(UtilityClass.class) class
// containing static method to be mocked
public class MockingStaticMethodTest {
    // the second thing You have to initialize
    // which class is HAVING the STATIC METHOD for MOCKING.
    // Ilgili ornekte UtilityClass
    // we would want to MOCK it

    @Mock
    Dependency dependency;

    @InjectMocks
    SystemUnderTest systemUnderTest;

    @Test
    public void testRetrieveTodosRelatedToSpring_usingMock() {
        List<Integer> stats = Arrays.asList(1, 2, 3);

        when(dependency.retrieveAllStats()).thenReturn(stats);

        PowerMockito.mockStatic(UtilityClass.class);

        when(UtilityClass.staticMethod(6)).thenReturn(150);

        int result = systemUnderTest.methodCallingAStaticMethod();

        assertEquals(150, result);

        // VERIFY the STATIC method was CALLED
        // Two Steps
        // 1. PowerMockito.verifyStatic()
        // 2. Actual Static Method call to verify

        PowerMockito.verifyStatic();
        UtilityClass.staticMethod(6);
    }
}
