https://github.com/in28minutes/full-stack-with-react-and-spring-boot
SECTION2 GETTING STARTED WITH REACT BASICS WITH COMPONENTS
3. STEP01 UNDERSTANDING FULL STACK APPLICATION ARCHITECTURE
we will start with basic AUTHENTICATION, and also discuss about how you can use JWTs, or JSON WEB TOKENS for authentication. We will use the spring security framework to build authentication around all the API, that we are going to build.

Why should you really build a full stack application? Why should you create a separate back end, and a separate front end. And there is a lot of complexity associated with this: You need multiple languages, You need multiple frameworks like Spring Boot, React., You need to learn all of that. Why should you really do that?
The answer to that question is that you would want to create a flexible architecture, an architecture which can be EXTENDED TO MEET YOUR FUTURE NEEDS.

4. STEP02 USING create-react-app TO CREATE AND LAUNCH A REACT APPLICATION
npm test  --> to run the unit test which are present in the project.
npm run build  --> to create a production deployable, where we write JavaScript code in modern javascript applications. We would create hundreds of JavaScript files and it not efficient in production to have hundreds of JavaScript files. So what npm run build does, is it merges everything into a compressed set of JavaScript files, which you can then use for production.

6. STEP03 IMPORTING REACT APP INTO VSCODE
Browser da SEKMEDEKI ISIM ve Kosede cikan ICON --> public/index.html altinda <title>React App</title>

7. STEP04 EXPLORING REACT PROJECT STRUCTURE
package.json EQUIVALENT pom.xml
react-scripts --> When we said npm start, all the work was done by the scripts.
node_modules --> Once you specify the dependencies in package.json, npm install them for you and it uses the folder node_modules. node_modules similar to the LOCAL MAVEN REPOSITORY, or even as the TARGET FOLDER where you have all the jars present.
index.css --> for the entire application so all the general CSS that we would want to include would include it in index.css.

index.html <div id="root"></div> ile index.tsx ReactDOM.render(<App />, document.getElementById('root')); AYNI ISIMLI OLMALI. IKISI de rootA, X, elma vs. OLMALI. Aksi takdirde hata aliriz.

8. STEP05 INTRODUCTION TO REACT COMPONENTS
Components help us to design and develop each of these individual elements as independent reusable pieces.
Component basically ALLOWS us to SPLIT the UI into INDEPENDENT REUSABLE PIECES of code.
Component
    view(JSX or Javascript)
    logic(javascript)
    styling(css)
    state(internal data state)
    props(pass data)

9. STEP06 PLAYING WITH REACT CLASS COMPONENTS
OPTION1: CLASS Component --> extends Component and implement render() method. Artik Kullanilmiyor

10. STEP07 INTRODUCTION TO FUNCTION COMPONENTS IN REACT
function ThirdComponent(){
    return (
        <div className="thirdComponent">
            Third Component
        </div>
    );
}
App.tsx icinde de <ThirdComponent/> ya da <ThirdComponent></ThirdComponent> seklinde cagiriyoruz.

11. STEP08 EXPLORING JSX FURTHER BABEL AND MORE
Babel is a javascript compiler. It enables us to write code in the latest version of JavaScript, and compile it or convert it to earlier versions of JavaScript. So if a browser does not support the latest version of JavaScript we can use the earlier version of JavaScript to run the code in there.

You cannot return two elements from a single JSX. JSX is like an expression; you can only just return one thing. Birden cok HTML node varsa <div></div> icine aliriz.

All NODES should be CLOSED. HATA aliriz

for CUSTOM DEFINED COMPONENTS, you have to use a CAPITAL letter as the STARTING letter. HATA aliriz. KUCUK HARFLE BASLAYAN html tagleriyle Karisir

whenever we use JSX, you need to have React in the import.

12. STEP09 REFACTORING COMPONENTS TO INDIVIDUAL MODULES AND QUICK REVIEW OF JAVASCRIPT
One of the BEST PRACTICES that is typically followed in React, is that EACH COMPONENT is DEFINED in ITS OWN file
any javascript file is called a MODULE.
the DEFAULT EXPORT, you can directly use it. For all other exports which you have from that specific class, you'd need to put them between CURLY BRACES. 


SECTION3 NEXT STEPS WITH REACT BUILDING COUNTER APPLICATION
17. STEP04 HANDLING CLICK EVENT ON THE INCREMENT BUTTON
One of the important things is in JSX, all the attributes so on click is an attribute on the button all the attribute use camel case. onClick