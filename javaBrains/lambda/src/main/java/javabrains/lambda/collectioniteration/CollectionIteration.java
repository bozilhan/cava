package javabrains.lambda.collectioniteration;

import javabrains.lambda.userdefinedlambda.Person;

import java.util.Arrays;
import java.util.List;

/*
 * https://www.youtube.com/watch?v=tfbmYBcq5CM&list=PLqq-6Pq4lTTa9YGfyhyW2CqdtW9RtY-I3&index=22
 * */

public class CollectionIteration {
    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
              new Person("Charles", "Dickens", 60),
              new Person("Lewis", "Carol", 42),
              new Person("Thomas", "Mann", 51),
              new Person("Barz", "Carzimillian", 51),
              new Person("Matthew", "Arnold", 51)
        );

        System.out.println("Using for loop");
        for (int i = 0; i < people.size(); i++) {
            System.out.println(people.get(i));
        }

        System.out.println("Using for-in loop");
        for (Person person : people) {
            System.out.println(person);
        }

        /*
         * forEach is a method that takes one argument which is a CONSUMER functional interface
         * it is an instance of CONSUMER functional interface
         *
         * FOR EACH ELEMENT in this people I want to EXECUTE a PARTICULAR LAMBDA expression
         *
         * using forEach helps processor to run multiple threads
         * */
        people.forEach(forEachElementInPeopleList -> System.out.println(forEachElementInPeopleList));

        /*
        * method reference version
        * */
        people.forEach(System.out::println);
    }
}
