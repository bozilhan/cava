package javabrains.lambda.functionalinterface;

/*
 * Java 8 Lambda Basics 19 - Closures in Lambda Expressions
 * https://www.youtube.com/watch?v=WcLum7g6ImU&list=PLqq-6Pq4lTTa9YGfyhyW2CqdtW9RtY-I3&index=19
 *
 * IMPORTANT!!!
 * Variable 'b' is accessed from within inner class, needs to be final or effectively final
 * 04:20/08:29
 * */
public class ClosuresJava7 {
    public static void main(String[] args) {
        int a = 10;
        final int b = 20;

        doProcess(a, new Process() {
            @Override
            public void process(int i) {
                /*
                 IMPORTANT!!!
                 Variable 'b' is accessed from within inner class, needs to be final or effectively final
                 04:20/08:29
                 b = 40;

                 EFFECTIVELY FINAL
                 EFFECTIVELY FINAL means that the compiler is saying
                 hey you dont have to put a `final` in a variable but
                 I am going to trust you that you ARE NOT going to CHANGE the variable
                 however I am still keeping an eye and
                 if you change the variable I am going to COMPLAIN
                * */
                System.out.println(i + b);
            }
        });
    }

    public static void doProcess(int i, Process process) {
        process.process(i);
    }
}

interface Process {
    void process(int i);
}
