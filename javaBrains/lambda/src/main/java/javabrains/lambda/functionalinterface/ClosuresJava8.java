package javabrains.lambda.functionalinterface;

/*
 * Java 8 Lambda Basics 19 - Closures in Lambda Expressions
 * https://www.youtube.com/watch?v=WcLum7g6ImU&list=PLqq-6Pq4lTTa9YGfyhyW2CqdtW9RtY-I3&index=19
 *
 *
 * EFFECTIVELY FINAL
 * EFFECTIVELY FINAL means that the compiler is saying
 * hey you dont have to put a `final` in a variable but
 * I am going to trust you that you ARE NOT going to CHANGE the variable
 * however I am still keeping an eye and
 * if you change the variable I am going to COMPLAIN
 * */
public class ClosuresJava8 {
    public static void main(String[] args) {
        int a = 10;

        /*
         * SCOPED VARIABLE used in your lambda expression
         * the compiler expects it to be EFFECTIVELY FINAL
         *
         * compiler says you DONT HAVE TO PUT a `final` there
         * AS LONG AS you can GUARANTEE that is `final`
         * I never COMPLAIN
         *
         * MAKE SURE you DONT CHANGE any VARIABLES that are in the enclosing scope inside the lambda
         * */
        int b = 20;

        doProcess(a, i -> System.out.println(i + b));
    }

    public static void doProcess(int i, Process process) {
        process.process(i);
    }
}
