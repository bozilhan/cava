package javabrains.lambda.functionalinterface;

/*
 * JAVA 8 LAMBDA BASICS 17 - EXCEPTION HANDLING IN LAMBDAS
 * Java 8 Lambda Basics 18 - An Exception Handling Approach IMPORTANT!!!
 * https://www.youtube.com/watch?v=SEGEbGoH4LI&list=PLqq-6Pq4lTTa9YGfyhyW2CqdtW9RtY-I3&index=17
 * */

import java.util.function.BiConsumer;

public class ExceptionHandling {
    public static void main(String[] args) {
        int[] numbers = {1, 2, 3, 4};
        int key = 0;

        /*
            process(numbers, key, (v, k) -> {
                try {
                    System.out.println(v / k);
                } catch (ArithmeticException e) {
                    System.out.println("ArithmeticException");
                }
            });
         */


        /*
         * there are multiple ways to handle this
         *
         * One approach is that
         * INSTEAD OF having the try-catch happen over here itself
         *
         * EXTERNALIZE try-catch into a SEPARATE function
         * and then WRAP System.out.println(v / k)
         * WITH ANOTHER LAMBDA that HAS try-catch
         * */
        process(numbers, key, wrapperLambda((v, k) -> System.out.println(v / k)));
    }

    private static void process(int[] numbers, int key, BiConsumer<Integer, Integer> biConsumer) {
        for (int number : numbers) {
            biConsumer.accept(number, key);
        }
    }

    private static BiConsumer<Integer, Integer> wrapperLambdaIllustration(BiConsumer<Integer, Integer> biConsumer) {
        /*
         * rather than passing (v, k) -> System.out.println(v / k) through
         * I am REPLACING (v, k) -> System.out.println(v / k) WITH (v, k) -> System.out.println(v + k)
         *
         * when process() method takes in the consumer process() method is taking (v, k) -> System.out.println(v + k)
         * so
         * consumer.accept() is called what gets executed is this print of the sum
         * */
        return (v, k) -> System.out.println(v + k);
    }

    /*
     * GOOD PLACE to DO try-catch
     *
     * you can do GENERIC wrapper lambda
     * */
    private static BiConsumer<Integer, Integer> wrapperLambda(BiConsumer<Integer, Integer> biConsumer) {
        return (v, k) -> {
            try {
                /*
                 * execute whatever is passed in
                 * */
                biConsumer.accept(v, k);
            } catch (ArithmeticException e) {
                System.out.println("Exception caught in the wrapper lambda");
            }
        };
    }
}
