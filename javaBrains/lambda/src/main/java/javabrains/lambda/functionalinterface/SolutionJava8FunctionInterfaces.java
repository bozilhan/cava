package javabrains.lambda.functionalinterface;

import javabrains.lambda.userdefinedlambda.Person;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class SolutionJava8FunctionInterfaces {
    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("Charles", "Dickens", 60),
                new Person("Lewis", "Carol", 42),
                new Person("Thomas", "Mann", 51),
                new Person("Barz", "Carzimillian", 51),
                new Person("Matthew", "Arnold", 51)
        );

        Collections.sort(people, (p1, p2) -> p1.getLastName().compareTo(p2.getLastName()));
        people.sort(Comparator.comparing(Person::getLastName));

        performConditionally(people, p -> true, System.out::println);
        performConditionally(people, p -> p.getLastName().startsWith("C"), p -> System.out.println(p.getFirstName()));
    }

    /*
     * if you were to ever need a lambda expression that takes in object and returns a boolean
     * you DONT HAVE TO CREATE A NEW INTERFACE
     * just USE PREDICATE INTERFACE
     *
     *
     * if you find an interface in the java.util.functions package
     * which pretty much addresses the lambda expression that you are writing well
     * you DONT HAVE TO CRETE the INTERFACE as well
     * you can just use these out-of-the-box interfaces
     */
    private static void performConditionally(List<Person> people, Predicate<Person> predicate, Consumer<Person> consumer) {
        for (Person p : people) {
            if (predicate.test(p)) {
                consumer.accept(p);
            }
        }
    }
}
