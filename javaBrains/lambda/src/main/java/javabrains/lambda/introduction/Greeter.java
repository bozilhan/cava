package javabrains.lambda.introduction;

public class Greeter {
    public void greet(Greeting greeting) {
        greeting.perform();
    }

    public static void main(String[] args) {
        Greeter greeter = new Greeter();

        Greeting lambdaGreeting = () -> System.out.println("Hello LambdaGreeting");

        /*
         * INLINE implementation of an interface
         * this is commonly REFERRED to as an ANONYMOUS INNER CLASS
         *
         * an INTERFACE WITH JUST ONE METHOD and
         * that was a PERFECT CANDIDATE for being USED AS a LAMBDA EXPRESSION
         * */
        Greeting innerClassGreeting = new Greeting() {
            @Override
            public void perform() {
                System.out.println("Hello InnerClassGreeting");
            }
        };

        greeter.greet(lambdaGreeting);
        greeter.greet(innerClassGreeting);

        MyAdd addFunction = (int a, int b) -> a + b;
    }
}

/*
 * creating an interface is ACTUALLY OPTIONAL
 * if you have an interface which ALREADY MATCHES
 * () -> System.out.println("Hello world");
 * i.e a method which doesnt take arguments and void type
 *
 * We have javabrains.lambda.introduction.Greeting interface which has void method with no arguments
 *
 * therefore we dont have to define javabrains.lambda.introduction.MyLambda and javabrains.lambda.introduction.MyAdd
 * */

/*
 * when you are using an interface to declare a lambda expression
 * that interface SHOULD HAVE ONLY ONE METHOD
 * and that method SHOULD BE HAVING THE SAME SIGNATURE AS the lambda expression
 * */

/*
 * interface name DOESNT MATTER
 * method name DOESNT MATTER
 * name of arguments doesnt matter
 *
 * signature of the lambda must match the signature of this method
 *
 * the interface which describes
 * () -> System.out.println("Hello world");
 * lambda expression
 * */
interface MyLambda {
    /*
     * the method which has EXACT same signature as this lambda expression
     * */
    void foo();
}

/*
 * interface name DOESNT MATTER
 * method name DOESNT MATTER
 * name of arguments doesnt matter
 *
 * signature of the lambda must match the signature of this method
 *
 * the interface which describes
 * (int a, int b) -> a + b;
 * lambda expression
 *
 * USAGE
 * javabrains.lambda.introduction.MyLambda myLambdaFunction = () -> System.out.println("Hello world");
 * */
interface MyAdd {
    /*
     * the method which has EXACT same signature as this lambda expression
     * */
    int foo(int x, int y);
}