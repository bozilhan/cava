package javabrains.lambda.introduction;

public interface Greeting {
    void perform();
}
