package javabrains.lambda.introduction;

/*
* in order to make sure none of your other team mates or
* other developers are going to go in there and
* add a method by mistake you use this annotation
*
* @FunctionalInterface
* gives a clue to any other developer that
* this is meant to be a functional interface
*
* any interface is considered a functional interface
* it there is ONLY ONE ABSTRACT METHOD and
* this kind of enforces it
*
*
* @FunctionalInterface annotation is COMPLETELY OPTIONAL
* the java compiler does NOT REQUIRE it for your lambda types
* BUT it is GOOD PRACTICE to ADD it
*
* to CONVEY your intention that a particular interface
* is MEANT TO BE USED as a FUNCTIONAL INTERFACE
*
* RECOMMEND TO USE it
* */
@FunctionalInterface

/*
 * When an interface will have EXACTLY 1 ABSTRACT method
 * we can say it as a FUNCTIONAL INTERFACE
 * */
public interface GreetingFunctionalInterface {
    /*
     * by default public abstract void perform()
     * */
    void perform();
}
