package javabrains.lambda.introduction;

public class HelloWorldGreeting implements Greeting {
    @Override
    public void perform() {
        System.out.println("Hello javabrains.lambda.introduction.HelloWorldGreeting");
    }
}
