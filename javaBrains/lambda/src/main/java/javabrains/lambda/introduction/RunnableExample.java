package javabrains.lambda.introduction;

public class RunnableExample {
    public static void main(String[] args) {

        Thread myThread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Printed inside runnable");
            }
        });

        /*
         * Runnable has a single method
         * It Runnable had more than one method
         * you could NOT have written a lambda function of that type
         *
         * these INTERFACES are called FUNCTIONAL INTERFACES
         *
         * an INTERFACE WITH JUST ONE METHOD and
         * that was a PERFECT CANDIDATE for being USED AS a LAMBDA EXPRESSION
         *
         * our lambda has EXACT signature with public void run()
         * i.e our lambda DOESNT TAKE ANY ARGUMENTS and has VOID type
         * */
        Thread myLambdaThread = new Thread(() -> System.out.println("Printed inside LAMBDA runnable"));

        myLambdaThread.start();
    }
}
