package javabrains.lambda.introduction;

public class TypeInferenceExample {
    public static void main(String[] args) {
        printLambda(s->s.length());
    }

    public static void printLambda(StringLengthLambda sll) {
        System.out.println(sll.getLength("hello lambda"));
    }

    /*
     * DEMONSTRATE string.length functionality
     * */
    interface StringLengthLambda {
        int getLength(String s);
    }
}
