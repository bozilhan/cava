package javabrains.lambda.methodreferences;

public class MethodReferences1 {
    public static void main(String[] args) {

        /*
         * () -> method()
         * ===
         * MethodReferences1::printMessage
         * */
        Thread t = new Thread(MethodReferences1::printMessage);
    }

    public static void printMessage() {
        System.out.println("hello");
    }
}
