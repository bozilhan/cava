package javabrains.lambda.methodreferences;

import javabrains.lambda.userdefinedlambda.Person;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

/*
* https://www.youtube.com/watch?v=lwwIZuwYmNI&list=PLqq-6Pq4lTTa9YGfyhyW2CqdtW9RtY-I3&index=21
*
* method references is an ALTERNATIVE SYNTAX for writing lambdas
* */

public class MethodReferences2 {
    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
              new Person("Charles", "Dickens", 60),
              new Person("Lewis", "Carol", 42),
              new Person("Thomas", "Mann", 51),
              new Person("Barz", "Carzimillian", 51),
              new Person("Matthew", "Arnold", 51)
        );

        Collections.sort(people, (p1, p2) -> p1.getLastName().compareTo(p2.getLastName()));
        people.sort(Comparator.comparing(Person::getLastName));

        /*
        * p-> System.out.println(p)
        * it is basically saying given a value of p, pass p to System.out.println()
        *
        * it is basically saying EXECUTE println() for EVERY VALUE
        * that is being sent to it
        *
        * p -> method(p)
        * ===
        * INSTANCE_NAME::METHOD_NAME
        *
        * when the java compiler gets this method reference java compiler realizes that
        * this is CONSUMER so java compiler basically takes in an input then passes it
        * */
        performConditionally(people, p -> true, p-> System.out.println(p));

        performConditionally(people, p -> true, System.out::println);

        performConditionally(people, p -> p.getLastName().startsWith("C"), p -> System.out.println(p.getFirstName()));
    }

    private static void performConditionally(List<Person> people,
                                             Predicate<Person> predicate,
                                             Consumer<Person> consumer) {
        for (Person p : people) {
            if (predicate.test(p)) {
                consumer.accept(p);
            }
        }
    }
}
