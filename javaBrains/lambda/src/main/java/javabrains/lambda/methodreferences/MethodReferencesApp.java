package javabrains.lambda.methodreferences;

/*
functional interface
*/
interface Calculator {
    void add(int num1, int num2);
}

class CalC {
    public static void addSomething(int num1, int num2) {
        System.out.println("addSomething " + num1 + " and " + num2 + " addition is " + (num1 + num2));
    }

    public void letsAdd(int num1, int num2) {
        System.out.println("letsAdd " + num1 + " and " + num2 + " addition is " + (num1 + num2));
    }
}

/*
functional interface
*/
interface Messenger {
    Message getMessage(String msg);
}

class Message {
    public Message(String msg) {
        System.out.println(">> Message is: " + msg);
    }
}

public class MethodReferencesApp {
    public static void main(String[] args) {
        /*
         * 1. reference to a static method
         * */
        Calculator cRef = CalC::addSomething;
        cRef.add(11, 22);

        /*
         * 2. reference to a non static method or instance method
         * */
        CalC calC = new CalC();
        Calculator cRefNonStatic = calC::letsAdd;
        cRefNonStatic.add(12, 23);

        /*
         * 3. Reference to constructor
         * */
        Messenger mRef = Message::new;
        mRef.getMessage("search the candle rather than cursing the darkness");
    }
}
