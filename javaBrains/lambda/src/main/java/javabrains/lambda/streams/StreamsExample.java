package javabrains.lambda.streams;

import javabrains.lambda.userdefinedlambda.Person;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/*
 * https://www.youtube.com/watch?v=0bHCxjkku0s&list=PLqq-6Pq4lTTa9YGfyhyW2CqdtW9RtY-I3&index=23
 * https://www.youtube.com/watch?v=suSdjhS03qk&list=PLqq-6Pq4lTTa9YGfyhyW2CqdtW9RtY-I3&index=24
 * stream is assembly line
 * every collections comes with a stream method
 *
 *
 * stream() involves three elements/parts
 * 1. SOURCE
 *    which provides the elements on that conveyor belt on that stream
 *    source ALWAYS HAS TO BE A BACKING COLLECTION
 * 2. OPERATIONS THAT NEED TO BE PERFORMED ON THAT stream()
 * 3. TERMINAL OPERATION/END CONDITION
 *    if you dont have end operation the stream() doesnt even get started
 * */

public class StreamsExample {
    public static void main(String[] args) {

        /*
         * you can think of list of objects as a horizontal line
         * */
        List<Person> people = Arrays.asList(
              new Person("Charles", "Dickens", 60),
              new Person("Lewis", "Carol", 42),
              new Person("Thomas", "Mann", 51),
              new Person("Barz", "Carzimillian", 51),
              new Person("Matthew", "Arnold", 51)
        );

        /*
         * you can think of stream() function AS A NEW VIEW of the COLLECTION
         *
         * the stream() ALWAYS HAS a SOURCE
         * in this case the source is a collection the list of `people`
         *
         * you can think of list of objects as a ASSEMBLY line
         *
         * we can PERFORM MULTIPLE METHODS ON EACH ELEMENT
         *
         *
         * putting all those person objects on a conveyor belt and streaming them
         *
         * THE FIRST WORKER on conveyor belt
         * .filter(predicate) only the elements that pass the predicate go to the next step in the assembly line to
         * the forEach
         * if the predicate says true then THAT ELEMENT REMAINS in the conveyor belt and then the next operation is
         performed
         *
         *if the predicate says false for any of those elements it just doesnt go to the next step
         *
         *THE SECOND WORKER on conveyor belt
         * .forEach(p -> System.out.println(p.getFirstName())
         */

        Stream<Person> streamReturnType = people.stream();

        people.stream()
                .filter(forEachElementInPeopleList -> forEachElementInPeopleList.getLastName().startsWith("C"))
                .forEach(forEachElementInPeopleList -> System.out.println(forEachElementInPeopleList.getFirstName()));

        long count = people.stream()
                .filter(forEachElementInPeopleList -> forEachElementInPeopleList.getLastName().startsWith("C"))
                .count();


        /*
         * PARALLEL STREAM
         *
         * */
        long parallelCount = people.parallelStream()
              .filter(forEachElementInPeopleList -> forEachElementInPeopleList.getLastName().startsWith("C"))
              .count();
    }
}
