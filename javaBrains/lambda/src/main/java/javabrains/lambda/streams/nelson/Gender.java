package javabrains.lambda.streams.nelson;

public enum Gender {
  MALE, FEMALE
}
