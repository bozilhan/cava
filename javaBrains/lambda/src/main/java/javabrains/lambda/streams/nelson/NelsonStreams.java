package javabrains.lambda.streams.nelson;

import java.util.*;
import java.util.stream.Collectors;

public class NelsonStreams {
    public static void main(String[] args) {
        List<Person> people = getPeople();

        /*
        IMPERATIVE APPROACH ❌
        */

        List<Person> femalesArrayList = new ArrayList<>();

        for (Person person : people) {
            if (person.getGender().equals(Gender.FEMALE)) {
                femalesArrayList.add(person);
            }
        }

        femalesArrayList.forEach(System.out::println);

        /*
        DECLARATIVE APPROACH ✅
        */

        /*
        Filter
        Filter takes a predicate which returns true or false
        */
        List<Person> females = people.stream()
              .filter(person -> person.getGender().equals(Gender.FEMALE))
              /*
               * collect the results back into a brand new list
               * */
              .collect(Collectors.toList());

        females.forEach(System.out::println);

        /*
        Sort
        */
        List<Person> sorted = people.stream()
              /*
               * pass the actual field that we want to compare
               * */
              .sorted(Comparator.comparing(Person::getAge)
                            .thenComparing(Person::getGender).reversed())
              .collect(Collectors.toList());

        sorted.forEach(System.out::println);

        /*
        ASK QUESTIONS ABOUT OUR COLLECTION
        all match, any match, none match

        All match
        EVERY ELEMENT should satisfy the condition
        */
        boolean allMatch = people.stream()
              .allMatch(person -> person.getAge() > 8);

        System.out.println(allMatch);

        /*
        Any match
        AT LEAST ONE ELEMENT should satisfy the condition
        */
        boolean anyMatch = people.stream()
              .anyMatch(person -> person.getAge() > 121);

        System.out.println(anyMatch);

        /*
        None match
        NO ONE ELEMENT should satisfy the condition
        */
        boolean noneMatch = people.stream()
              .noneMatch(person -> person.getName().equals("Antonio"));

        System.out.println(noneMatch);

        /*
        Max
        */
        people.stream()
              /*
               * max returns Optional
               * max() might not find the maximum value within this collection
               * */
              .max(Comparator.comparing(Person::getAge))
              .ifPresent(System.out::println);

        /*
        Min
        */
        people.stream()
              .min(Comparator.comparing(Person::getAge))
              .ifPresent(System.out::println);

        /*
        Group
        */
        Map<Gender, List<Person>> groupByGender = people.stream()
              .collect(Collectors.groupingBy(Person::getGender));

        groupByGender.forEach((gender, people1) -> {
            System.out.println(gender);
            people1.forEach(System.out::println);
            System.out.println();
        });


        Optional<String> oldestFemaleAge = people.stream()
              .filter(person -> person.getGender().equals(Gender.FEMALE))
              .max(Comparator.comparing(Person::getAge))
              /*
              * map() allows me to do a TRANSFORMATION on the actual value
              * */
              .map(Person::getName);

        oldestFemaleAge.ifPresent(System.out::println);
    }

    private static List<Person> getPeople() {
        return List.of(
              new Person("Antonio", 20, Gender.MALE),
              new Person("Alina Smith", 33, Gender.FEMALE),
              new Person("Helen White", 57, Gender.FEMALE),
              new Person("Alex Boz", 14, Gender.MALE),
              new Person("Jamie Goa", 99, Gender.MALE),
              new Person("Anna Cook", 7, Gender.FEMALE),
              new Person("Zelda Brown", 120, Gender.FEMALE)
        );
    }
}
