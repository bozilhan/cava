package javabrains.lambda.userdefinedlambda;

public interface MyCondition {
    boolean test(Person p);
}
