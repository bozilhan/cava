package javabrains.lambda.userdefinedlambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SolutionJava7 {
    public static void main(String[] args) {

        List<Person> people = Arrays.asList(
                new Person("Charles", "Dickens", 60),
                new Person("Lewis", "Carol", 42),
                new Person("Thomas", "Mann", 51),
                new Person("Barz", "Carzimillian", 51),
                new Person("Matthew", "Arnold", 51)
        );

        /*
         * Step1: sort this list by lastName
         *
         * COMPARATOR is interface which NEEDS IMPLEMENTATION of the compare() method
         * since there is ONLY ONE METHOD here so Comparator is FUNCTIONAL INTERFACE
         *
         * since Comparator is a functional interface
         * you can actually pass in LAMBDA
         * INSTEAD OF this INLINE ANONYMOUS INNER CLASS
         * */
        Collections.sort(people, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getLastName().compareTo(o2.getLastName());
            }
        });

        /*
         * Step2: create a method that prints all elements in the list
         * */
        printAll(people);

        /*
         * Step3: create a method that prints all people that have last name beginning with C
         *
         * FILTER OUT anybody who DOES NOT HAVE the last name BEGINNING with C
         * */
        printLastNameBeginningWithC(people);

        printConditionally(people, new MyCondition() {
            @Override
            public boolean test(Person p) {
                /*
                * test passes(is true) only if the lastName begins with "C"
                * */
                return p.getLastName().startsWith("C");
            }
        });
    }

    private static void printConditionally(List<Person> people, MyCondition myCondition) {
        for (Person p : people) {
            if (myCondition.test(p)) {
                /*
                 * calls toString() method of Person class
                 * */
                System.out.println(p);
            }
        }
    }

    private static void printLastNameBeginningWithC(List<Person> people) {
        for (Person p : people) {
            if (p.getLastName().startsWith("C")) {
                /*
                 * calls toString() method of Person class
                 * */
                System.out.println(p);
            }
        }
    }

    private static void printAll(List<Person> people) {
        for (Person p : people) {
            /*
             * calls toString() method of Person class
             * */
            System.out.println(p);
        }
    }
}
