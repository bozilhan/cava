package javabrains.lambda.userdefinedlambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SolutionJava8 {
    public static void main(String[] args) {

        List<Person> people = Arrays.asList(
                new Person("Charles", "Dickens", 60),
                new Person("Lewis", "Carol", 42),
                new Person("Thomas", "Mann", 51),
                new Person("Barz", "Carzimillian", 51),
                new Person("Matthew", "Arnold", 51)
        );

        /*
         * Step1: sort this list by lastName
         *
         * Collections KNOWS THE TYPE OF THIS LIST people which is a list of person
         * so we DONT HAVE to SPECIFY the type of p1 and p2
         */
        Collections.sort(people, (p1, p2) -> p1.getLastName().compareTo(p2.getLastName()));
        /*
        * EQUIVALENT TO THE ABOVE
        * */
        people.sort(Comparator.comparing(Person::getLastName));

        /*
         * Step2: create a method that prints all elements in the list
         *
         * always returns true therefore printAll elements in the list
         * */
        printConditionally(people, p -> true);

        /*
         * Step3: create a method that prints all people that have last name beginning with C
         *
         * FILTER OUT anybody who DOES NOT HAVE the last name BEGINNING with C
         * */
        printConditionally(people, p -> p.getLastName().startsWith("C"));
    }

    private static void printConditionally(List<Person> people, MyCondition myCondition) {
        for (Person p : people) {
            if (myCondition.test(p)) {
                /*
                 * calls toString() method of Person class
                 * */
                System.out.println(p);
            }
        }
    }
}
