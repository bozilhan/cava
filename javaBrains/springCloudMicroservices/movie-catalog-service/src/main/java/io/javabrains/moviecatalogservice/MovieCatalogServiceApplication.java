package io.javabrains.moviecatalogservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient

/*
* this application now has hystrix enabled
* */
@EnableCircuitBreaker
public class MovieCatalogServiceApplication {

    public static void main(String[] args) {
        /*
         * the reason Tomcat server runs here is because we have CHOSEN starter-WEB dependency
         * starter web dependency brings in tomcat by default
         * */
        SpringApplication.run(MovieCatalogServiceApplication.class, args);
    }

    /*
     * RestTemplate does service discovery in a load balanced way.
     * We tell RestTemplate DONT GO TO a SERVICE DIRECTLY.
     * WHATEVER URL I AM GIVING YOU IT IS NOT ACTUAL URL THE URL I AM GIVING YOU IS BASICALLY HINT ABOUT WHAT SERVICE
     *  YOU NEED TO DISCOVER.
     * RestTemplate will look for hints which service to call when we give it the url
     * */
    @LoadBalanced
    /*
     * @Bean is PRODUCER Spring executes the method and saves the instance somewhere
     * */
    @Bean
    public RestTemplate getRestTemplate() {
        /*
        * WITHOUT CIRCUIT BREAKER
         * First model of setting timeout NOT BEST PRACTICE
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
         * 3000 ms
        clientHttpRequestFactory.setConnectTimeOut(3000);
         *  as long as the response comes back within three seconds it is all good
         * if response doesnt get back within 3 seconds restTemplate THROWN ERROR
        return new RestTemplate(clientHttpRequestFactory);
         * */
        return new RestTemplate();
    }

    /*
    @Bean
    public WebClient.Builder getWebClientBuilder() {
        return WebClient.builder();
    }*/
}

