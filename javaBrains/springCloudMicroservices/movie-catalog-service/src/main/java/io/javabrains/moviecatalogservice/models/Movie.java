package io.javabrains.moviecatalogservice.models;

/*
* it is perfectly OK to create copies of these classes WITH DEFAULT CONSTRUCTOR
* when we have java Unmarshall(Converting XML/JSON to Objects) something which is NOT AN OBJECT
* to AN OBJECT we need to PROVIDE EMPTY CONSTRUCTOR

 * */
public class Movie {
    private String movieId;
    private String name;
    private String description;

    public Movie() {

    }

    public Movie(String movieId, String name, String description) {
        this.movieId = movieId;
        this.name = name;
        this.description = description;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
