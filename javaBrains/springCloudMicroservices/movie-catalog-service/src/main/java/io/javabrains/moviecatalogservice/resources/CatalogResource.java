package io.javabrains.moviecatalogservice.resources;

import io.javabrains.moviecatalogservice.models.CatalogItem;
import io.javabrains.moviecatalogservice.models.UserRating;
import io.javabrains.moviecatalogservice.service.MovieInfo;
import io.javabrains.moviecatalogservice.service.UserRatingInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.stream.Collectors;

@RestController

/*
 * when somebody calls this url
 * load up(yuklemek) this class
 * */
@RequestMapping("/catalog")
public class CatalogResource {
    /*
     * @ Bean the factory method of restTemplate has been defined in MovieCatalogServiceApplication
     * we want to have one instance and use that for multiple calls
     * */
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    WebClient.Builder webClientBuilder;

    /*
     * Refactor for granular fallback
     * Two services that we need to create
     * */
    @Autowired
    MovieInfo movieInfo;

    @Autowired
    UserRatingInfo userRatingInfo;

    /*
    * To get multiple instances of the same microservice
    * we have to add DiscoveryClient in our classpath
    *
    * NOT RECOMMENDED!!!
    @Autowired
    private DiscoveryClient discoveryClient;
    * */

    /*
     * /catalog/barz
     * call this method
     *
     * {} -> tells springboot that this not literally(kelimenin tam anlami, harfi harfine)
     * matching a `userId` IT IS A VARIABLE
     *
     *
     * @PathVariable -> to pass @RequestMapping variable to a method
     *
     *
     * this method runs every time the request comes in
     *
     *
     * MAKING MULTIPLE MICROSERVICE CALLS THEREFORE CIRCUIT BREAKER SHOULD NEED
     * POSSIBLE CONTENDER(yarismaci) FOR A HISTORIC CIRCUIT BREAKER
     *
     *
     * if have one method which is calling 2 apis if any of those api fails we will break the circuit
     * we want to have more granularity(taneciklik) in your fallback mechanism.
     * we will split the main api into 2 methods one method that calls each api and
     * we will have INDIVIDUAL FALLBACKS FOR EACH API and then
     * the main api DOESNT NEED FALLBACK because all the APIs are covered they have their own fallbacks
     * */
    @RequestMapping("/{userId}")
    public List<CatalogItem> getCatalog(@PathVariable("userId") String userId) {
        /*
         * get all rated movie IDs
         * is there any way to pass array of movie Ids and get the response in a single go?
         * Iterative connections to the REST API can be costly.
         *
         * getForObject() -> its first argument is URL that we want to call
         * the URL DOSENT HAVE TO BE A MICROSERVICE. URL will make a REST CALL
         * we can provide a class which HAS the SAME PROPERTIES AS THE JSON and
         * getForObject("http://localhost:8081/movies/foo" + rating.getMovieId(), Movie.class);
         *
         * http://ratings-data-service/ratingsdata/user/
         * eureka just knows instances currently registered with eureka
         * ratings-data-service -> the name which we define in application.properties of ratings-data-service
         * microservice
         * RATHER THAN localhost:8081
         * */
        UserRating userRating = userRatingInfo.getUserRating(userId);

        /*
        for each movie id, call movie-info-service and get details
        localhost:8082 -> movie-info-service (the name which we define in application.properties of
        movie-info-service microservice)
        */
        return userRating.getRatings().stream()
                .map(rating -> movieInfo.getCatalogItem(rating))
                .collect(Collectors.toList());
    }
}

/*
Alternative WebClient way
webClientBuilder.build() -> using builder pattern and give us a client
retrieve() -> go to the fetch
bodyToMono() -> is basically saying whatever body you get back convert it into an instance of this Movie.class
mono is like a kind of PROMISE
block() -> converts asynchronous from synchronous
Movie movie = webClientBuilder.build().get().uri("http://localhost:8082/movies/"+ rating.getMovieId())
.retrieve().bodyToMono(Movie.class).block();
*/