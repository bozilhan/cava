package io.javabrains.moviecatalogservice.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import io.javabrains.moviecatalogservice.models.Rating;
import io.javabrains.moviecatalogservice.models.UserRating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

/*
 * we have separate class which is making the call AS THE FALLBACK
 * */
@Service
public class UserRatingInfo {
    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "getFallbackUserRating",
    commandProperties = {
            @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds",value = "2000"),
            @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds",value = "2000"),
    })
    public UserRating getUserRating(String userId) {
        return restTemplate.getForObject("http://ratings-data-service/ratingsdata/user/" + userId,
                                         UserRating.class);
    }

    /*
     * fallback method needs to be simple HARD-CODED responses or
     * the most we want to do pick something from the CACHE
     * KEEP THING SIMPLE for the fallback
     * we want to REDUCE the POSSIBILITY of an error when a fallback method executes
     * */
    public UserRating getFallbackUserRating(String userId) {
        /*
         * COMPLETELY HARD-CODED
         * */
        UserRating userRating = new UserRating();
        userRating.setUserId(userId);
        userRating.setRatings(Arrays.asList(
                new Rating("0", 0)
        ));
        return userRating;
    }
}
