http://www.luv2code.com/
SECTION 70
507. WHAT IS SPRING BOOT
WAR file
you only have your code included. There's no need to have the EMBEDDED server, because now you're deploying it in a traditional sense.
There's already a Tomcat server installed running elsewhere and we're simply deploying our WAR file to that server.


508. SPRING INITIALIZR
https://www.youtube.com/watch?v=lhkwLtDIMHI&feature=youtu.be
it's a website where you can quickly create a started Spring project, so it's at start.spring.io.
We basically go here and select our DEPENDENCIES,
it'll actually create a Maven/Gradle project for us, and then
we can actually import that project into our IDE like Eclipse, IntelliJ, NetBeans, and so on.
    Development Process
    1. Configure our project at Spring initializr website
        choose the LATEST RELEASED springboot version
    2. download the zip file
    3. unzip the file
    4. import MAVEN project into our ide


509. DEVELOPING REST API CONTROLLER
1. ROOT package altina rest isminde package tanimla
2. rest pacakage altina Controller sinif tanimla XXXRestController


510. SPRINGBOOT PROJECT STRUCTURE
src/main/java       --> your source code
src/main/resources  --> Properties/config files used by your app
src/test/java       --> unit testing source code
maven Wrapper files --> mvnw, mvnw.cmd, mvnw.sh
    mvnw
        they allow you to run a Maven project, and the really cool thing about it is that there's NO NEED to have Maven INSTALLED, or have it PRESENT on your PATH.
        So what these files will do is they'll say hey, if the correct version of Maven is not found on your computer, then it'll actually automatically download the correct version of Maven, from the Internet and then actually run Maven, so it'll basically do some smarts there, it'll automatically download it and then run it.

    mvnw.cmd/mvnw.sh
        So there's actually two files that are provided for you, you have one of them that's the mvnw.cmd file, that's for Microsoft Windows, so you can use that to say mvnw and then you know, normal Maven self like clean compile test or whatever. And then there's the mvnw.sh and this is for Linux or Mac systems, so here you can say./mvnw clean compile test and so on. So that's the basic idea here of the mvnw files, they're basically wrapper files that'll automatically download, and run the correct version of Maven, for you. Now, if you already have Maven installed in your computer, then there's actually no need for the mvnw files, so you can safely ignore those files, or delete them they're not required. So in that case, since you ALREADY have Maven LOCALLY INSTALLED, you simply use Maven as you normally would so here you'd say mvn clean compile test, so notice here there's no mvnw, you simply use mvn by itself. Alright so that's the mvnw commands, or the Maven Wrapper files

maven POM file pom.xml
    pom.xml is the place where you would declare all the DEPENDENCIES that you would want.


@SpringBootApplication
    enables Auto configuration, Component scanning, additional configuration of your application
@SpringBootApplication composed of
    @EnableAutoConfiguration
        Enables Spring boot's auto-configuration
    @ComponentScan
        Enables component scanning of current package
        Also recursively scans sub-packages
    @Configuration
        Able to register extra beans with @Bean or import other configuration classes

org.springframework.boot.SpringApplication
    it is a special class that we use to Bootstrap our spring application
    Spring Boot will actually CREATE the APPLICATION CONTEXT,
    REGISTER all the BEANS to all the component scanning,
    and it'll also START the EMBEDDED TOMCAT server, and it'll get that SERVER UP and RUNNING,

src/main/resources/application.properties
    we can add some springboot properties
        server.port=8585
        you're kind of telling Spring Boot, hey, don't listen on port8080, I want you to listen on port8585
    luv2codeSpringBoot/FunRestController sinifinda nasil okundugu mevcut
        @Value annotationuyla istedigimiz custom propertyi siniflarimiza inject edebiliriz

src/main/resources/static
    static resources are like HTML files, cascading styles sheets CSS, JavaScript, images, PDFs etc.
    So you simply place them to the static directory and Spring Boot will load it AUTOMATICALLY for your given application.

src/main/resources/template
    Template engineler FreeMarker, Thymeleaf, Mustache yer alir. En populeri Thymeleaf

BEST PRACTICE
    Place your main application class in the ROOT package ABOVE your other packages
        Main spring boot application class AUTOMATICALLY component scans sub-packages (mevcut ornekte rest package)
        // @SpringBootApplication parametresi olarak
        @SpringBootApplication(
            scanBasePackages={"org.acme.iot.utils", "edu.cmu.wean"})

    do not USE the src/main/webapp directory if your application is packaged as a JAR.
        although this is a standard Maven directory, it only works with WAR packaging.
        it's silently IGNORED by MOST BUILD TOOLS if you generate a JAR file,
        and believe me you'll WASTE MANY HOURS, MANY HOURS trying to RESOLVE this or trying to get this to WORK.


512. SPRINGBOOT STARTERS
Springboot starters is a COLLECTION of Maven DEPENDENCIES. It actually contains other dependencies.
it contains Spring Web, Spring Web MVC, Hibernate Validator, JSON, Tomcat and so on.
This saves the developer from having to list all of the individual dependencies and it also makes sure that you have compatible versions of those dependencies.
REDUCES the amount of Maven configuration
        maven pom.xml e sunu ekliyoruz
            <dependencies>
                ...
                <dependency>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-starter-web</artifactId>
                </dependency>
                ....
            </dependencies>
springboot initializr da web dependencyi sectigimizde spring-boot-starter-web otomatik olarak pom.xmle eklenir.
springboot initializr dan hangi dependencyi secersek ilgili dependency icin gerekenler otomatik olarak pom.xml e eklenir.
30'dan fazla springboot starter mevcuttur 04:48/07:15
=================================================================================================================================
=================================================================================================================================
=================================================================================================================================
SECTION 71
https://dev.to/suin/spring-boot-developer-tools-how-to-enable-automatic-restart-in-intellij-idea-1c6i
514. SPRINGBOOT DEV TOOLS
this will automatically restart our application when our code is updated and it's really easy.
We simply need to just ADD A SPECIAL DEPENDENCY to our maven POM file.
There's no need to write any additional code. So there's a lot of Spring Magic that works in the background for us.
    1. Edit pom.xml
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
		</dependency>
    2. add new REST endpoint to our app
    3. verify the app is automatically reloaded


515. SPRINGBOOT ACTUATOR
Spring Boot Actuator actually EXPOSES endpoints for you to MONITOR and MANAGE your application,
so you easily get the DevOps functionality out-of-the-box,
you simply add the dependency to your Maven POM file and then these REST endpoints are automatically added to your application,
so the nice thing about it is that there's no need to write additional code
    Chrome extension JSON Formatter
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
 10 dan fazla actuator mecvuttur
        /actuator/health        --> this gives you health information about your application
        /actuator/info          --> that's information about your given project
        /actuator/beans         --> list of all spring beans registered with your application. USEFUL for DEBUGGING CONFIGURATION
        /actuator/threaddump    --> list of all threads running in your application. USEFUL for ANALYZING and PROFILING your applications's performance
        /actuator/mappings      --> list of all of the request mappings for your app. USEFUL for FINDING OUT WHAT REQUEST MAPPINGS ARE AVAILABLE
 src/main/resources/application.properties e /actuator/info yu degistirecek propertyler ekleyebiliriz


516. ACCESSING ACTUATOR ENDPOINTS
use wildcard "*" to expose all endpoints
can also expose individual endpoints with a comma-delimited list
management.endpoints.web.exposure.include=*


517. APPLYING SECURITY TO ACTUATOR ENDPOINTS
we may not want to expose all of this information to just anyone on the web.
So what we'd like to do is add Spring Security to our project and secure our endpoints.
/health and /info are still available. You can disable them if you want
    First Approach
        userName: user
        password: Run ekraninda yazan Using generated security password: 9e11b6e3-c022-43ae-bee1-4f5270494480
    Second Approach
        src/main/resources/application.properties
        spring.security.user.name=scott
        spring.security.user.password=tiger
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-security</artifactId>
        </dependency>
you can customize Spring Boot Actuator endpoints using Spring Security.
# exclude individual endpoints with a comma-delimited list
management.endpoints.web.exposure.exclude=health,info
no one can access those given endpoints.
=================================================================================================================================
=================================================================================================================================
=================================================================================================================================
SECTION 72
518. RUNNING SPRINGBOOT APPLICATION FROM COMMAND LINE
    1. mvnw spring-boot:run jar file olusturmaya GEREK YOK!!!
    2. mvnw package --> ./mvnw package -DskipTests
        Create jar file for our application. And this JAR file is created in the target sub-directory.
        mvnw allows you to run a maven project
        no need to have maven installed or present on your path
        If correct version of maven is NOT found on your computer. Automatically downloads correct version and runs maven
        if you already have maven installed JUST use mvn package
            2.1. java -jar {BIR_ONCEKI_ADIMDA_OLUSAN_JAR_FILE_ISMI}.jar
=================================================================================================================================
=================================================================================================================================
=================================================================================================================================
SECTION 73
520. SPRINGBOOT INJECTING CUSTOM APPLICATION PROPERTIES
You need for your app to be CONFIGURABLE. NO HARD-CODING of values
You need to READ app configuration from properties file src/main/resources/application.properties
Spring Boot app can access these properties simply using the @Value annotation.


521. CONFIGURING THE SPRINGBOOT SERVER
src/main/resources/application.properties e server port, context path, actuator, security etc. ekleme
the categories of Core, Web, Security, Data, Actuator, Integration, DevTools, and Testing.
Core
    logging.XXX
    setup logfile name
    setup logging level of springframework, hibernate, our application etc.
Web
    HTTP server.port
        server.port=7070
    Context path of the application. DEFAULT context path is /
        server.servlet.context-path=/my-silly-app
    http://localhost:7070/my-silly-app/fortune
=================================================================================================================================
=================================================================================================================================
=================================================================================================================================
SECTION 74
REST CRUD API WITH HIBERNATE
Various DAO Techniques
    1. Use EntityManager but leverage native Hibernate API
522. OVERVIEW AND DATABASE SETUP
    REST API with springboot that CONNECTS to database
    POST /api/employees                 --> CREATE a NEW employee
    GET /api/employees                  --> READ a LIST OF employees
    GET /api/employees/{employeeId}     --> READ a single employee
    PUT /api/employees                  --> UPDATE an EXISTING employee
    DELETE /api/employees/{employeeId}  --> DELETE an EXISTING employee
        1. SETUP DATABASE Dev Environment
        2. CREATE springboot project USING Spring Initializr
        3. GET list of employees
        4. GET single employee by ID
        5. ADD a new employee
        6. UPDATE an existing employee
        7. DELETE an existing employee


523. CREATE springboot project USING Spring Initializr
    Dependencies
        Spring Web
        Spring Data JPA
        Spring Boot DevTools
        MySQL Driver

524. INTEGRATING HIBERNATE AND JPA
Build DAO(Data Access Object) Layer and Hibernate
Springboot will AUTOMATICALLY CONFIGURE your data source for you. Basen on entries from maven pom file
    Spring Data (ORM): spring-boot-starter-data-jpa
    JDBC Driver: mysql-connector-java
DB Connection info from application.properties
    set up the URL,
    you set up the username and the password.
    # JDBC properties
    # Springboot will automatically create beans for DataSource and EntityManager
    spring.datasource.url=jdbc:mysql://localhost:3306/employee_directory?useSSL=false&serverTimeZone=UTC
    spring.datasource.username=root (ORNEK)
    spring.datasource.password=rootroot (ORNEK)
    And then there's no need to give the JDBC driver class name. Spring Boot will automatically detect it based on the URL.

What is JPA?
    JPA is the standard API for object-to-relational-mapping.
    It's only a specification. It defines a set of interfaces, but it actually requires an implementation to be usable.
    HIBERNATE is probably the most popular implementation of the JPA specification.
    In springboot Hibernate is default implementation of JPA
    EntityManager is similar to Hibernate SessionFactory
        EntityManager can serve as a wrapper for a Hibernate SESSION object
        We can inject the EntityManager into our DAO

Various DAO Techniques
    1. Use EntityManager but leverage native Hibernate API
    2. Use EntityManager and standard JPA API
    3. Spring Data JPA


525. CONFIGURING THE SPRINGBOOT DATA SOURCE
    application.properties icine JDBC(Java Data Base Connectivity) tanimlarini yapiyoruz.
        Springboot will automatically create beans for DataSource and EntityManager
    create a new package 'entity'
    create DB'deki ilgili TabloAdinda CONCRETE class (ilgili ornekte Employee tablosu oldugundan Employee concrete sinifi tanimladik) entity to this package
        what we need to do here is just CREATE an ENTITY and MAP this entity to our given database table.
        !!! ID's the primary key so I'll make use of the @Id and @generated value. !!!
        !!! I'll make use of the @Column annotation to map these fields to a given database column name. !!!
        member degiskenler icin column nameleri yaziyoruz. this actually has to be the COLUMN NAME!!!
               TABLODAKI KOLON ADI NEYSE AYNISINI YAZIYORUZ.
        NO-ARG constructor is REQUIRED by Hibernate.
        And then I'll also create a constructor USING THE FIELDS. I'll select of them EXCEPT ID, 'cause ID's going to be AUTOMATICALLY GENERATED, so have first name, last name and email. So those are the three that you should have selected for this example.
        Define GETTER and SETTER for member variables
        Override toString() method


526. DEVELOPING THE DAO INTERFACE AND IMPLEMENTATION
    Create new package for DAO 'dao'
    Create DAO INTERFACE DBdekiTabloIsmiDAO interfacei
        crud islemlerini gerceklestiren metodlarin signaturei
        All members of interfaces lacking access modifiers are implicitly public
            List<Employee> findAll();
            Employee findById(int theId);
            void save(Employee theEmployee); // UPDATE
            void deleteById(int theId);
    Create CONCRETE class implements DBdekiTabloIsmiDAO --> DBdekiTabloIsmiDAOImpl
        we defined the fields for our EntityManager and also made use of constructor injection, and again, you can choose any type of injection that you'd like there. And then we moved down and we implemented the findAll method so making use of that @Transactional for transaction management. And we simply get the session, create the query, execute, and return the results.


527. CREATING REST CONTROLLER METHODS
Create our REST controller to use our DAO
    Create new package for REST 'rest'
    Create REST controller(Concrete Class) TO USE DAO


528. ADDITIONAL CRUD METHODS FOR DAO FIND, ADD, UPDATE AND DELETE
DAO INTERFACE --> crud projesi IEmployeeDAO interfacee abstract metodlari ekle
CONCRETE EmployeeDAOHibernate sinifinda ilgili metodlari implement et


529. REFACTORING THE CODE TO USE A SERVICE LAYER
Create new package for the SERVICE 'service'
Create new INTERFACE IEmployeeService
     define the same methods that we wrote at the DAO layer IEmployeeDAO.
Create a CONCRETE CLASS for the SERVICE IMPLEMENTATION EmployeeService
Move REST CONTROLLER and USE the EmployeeService


530. REST CONTROLLER METHODS TO FIND AND ADD EMPLOYEE
Read a SINGLE EMPLOYEE
EmployeeRestController GET /employees/{employeeId}
POST


SECTION 75
VERSION2: BUILD A REST CRUD WITH JPA
    Various DAO Techniques
        2. Use EntityManager and standard JPA API
532. REST OVERVIEW
The JPA API methods are similar to Native Hibernate API
Maintain portable, flexible code
JPA also supports a QUERY LANGUAGE JPQL
    createQuery() metoduna string olarak verilen query parametresi JPQL query language ile yaziliyor
Build DAO layer TableNameDAO Interfaceinden concrete sinif olusturma
Using Standard JPA API


533. CREATING JPA DAO IMPLEMENTATION FOR REST API
07:05/10:27 COMPILATION ERROR AND ITS SOLUTION !!!
    Description: EmployeeServiceImpl required a single bean but 2 were found
    Action: Consider marking one of beans as @Primary, updating the consumer to accept multiple beans, or using @Qualifier to identify the bean that should be consumed
    !!!Version2 projesini Version1'den KOPYALADIGIMIZDAN EmployeeDOA Interfaceinin 2 ADET concrete class implementationu oldu (Version1 icin olani SILMEDIK YA DA UNUTTUK) Service Layer a da EmployeeDOA interface inject ettigimizden service layer 2 adet concrete sinif implementationundan HANGISINI ALACAGINI BILEMEDI. Hata muhtemelen bundan kaynaklandi.


SECTION 76
VERSION3: SPRING DATA JPA OVERVIEW
Various DAO Techniques
    3. Spring Data JPA
535. Creating Spring Data JPA Repository
OLDUGU GIBI KOPYALA VE OKU


536. CREATING SPRING DATA JPA REPOSITORY
Version1 ve Version2'ye yer alan 'dao' packageinin altindaki TUM interface ve concrete siniflari SIL!!!
1. Create Spring Data JPA Repository NEW INTERFACE TableIsmiRepository
    So we should have our Interface EmployeeRepository extends JpaRepository, and then for the Entity type, I'll say Employee, and then for the actual Primary key type I'll give Integer, because our Employee class is based on an Integer Primary key.
    public interface TabloIsmiRepository extends JpaRepository<{ENTITY_TYPE}, {PRIMARY_KEY_TYPE}> {} generic interface!!!
    there's no need to write any code. We get these CRUD methods for free, and there's no need to write an implementation class, that's all the beauty of spring data jpa.
04:25/10:24
2. Use repository in our app. {TableIsmi}ServiceImpl sinifi DEGISECEK!!!
    Constructor injectionumuz degistiginden {TableIsmi}ServiceImpl sinifindaki member degiskenimizin tipi {TableIsmi}Repository oluyor. (ZATEN {TableIsmi}DAO Interface KALDIRILDI!!!)
    2.1. {TableIsmi}ServiceImpl deki tum mevcut crud metodlarinda {TableIsmi}Repositorynin ilgili metodunu cagiriyoruz. DELEGATE to JpaRepository's methods!!!
        07:25/10:24 findById metodu DEGISIYOR!!!
    2.2. {TableIsmi}ServiceImpl'de Version1 ve 2de kullanilan @Transactional annotationu KALDIRILIYOR.
        we can REMOVE the @Transactional, since JpaRepository PROVIDES this FUNCTIONALITY out of the box, so NO NEED to list it here.


SECTION 77
SPRING DATA REST
create a REST API for me. Use my existing JpaRepository, my entity and primary key, and give me all of the basic REST API CRUD features for free.
Spring Data Rest is the solution. It's a separate Spring project. You can use it with regular Spring or Spring Boot. It leverages your existing JpaRepository, and Spring will give you a REST CRUD implementation for free.
Development Process
    1. Add spring data rest to your Maven POM file. THAT'S IT :). NO CODING required. Spring Data REST will scan for JpaRepositories and expose endpoints. A    nd so you'll get these endpoints for free based on the various entity types in your JpaRepositories.
        <!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-data-rest -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-rest</artifactId>
            <version>2.2.0.RELEASE</version>
        </dependency>
In a nutshell (KISACA)
    For Spring Data REST, you only need 3 items
    1. Your ENTITY --> {TabloIsmi}
    2. JpaRepository --> {TabloIsmi}Repository extends JpaRepository
    3. Maven POM dependency for --> spring-boot-starter-data-rest
BEFORE Spring Data REST, we had our REST controller. We had our employee SERVICE. We had our SERVICE IMPLEMENTATION. So, about THREE FILES and 100 plus lines of code,
AFTER Spring Data REST, you're going to luv this right, no coding required. Just give a Maven POM entry. Spring Data REST will scan for those repositories and automatically expose those endpoints for free.
Spring Data REST also has some advanced features, so there's support for pagination, sorting, and searching. You can also extend and add custom queries with JPQL. You can also customize the REST API by making use of the Query Domain Specific Language or the Query DSL.


539. SPRING DATA REST CREATING REST API ONEMLI!!!
1. Delete CONTROLLER AND SERVICE PACKAGES. yeni projede de EKLEMIYORUZ!!!
spring.data.rest.base-path=/magic-api


540. SPRING DATA REST CONFIGURATION PAGINATION AND SORTING ONEMLI!!!
spring data resource pathde ilgili tablo icin sonuna INGILIZCE COGUL EKI KOYAR
Employee Tablosu icin /employees
{TabloIsmi} --> /{TabloIsmi}s
Spring Data REST does not handle complex pluralized forms, so in this case, you need to manually specify a plural name. Also, what if we wanted to expose a different resource name? So instead of /employees, we wanted to use something like /members. Here's the actual solution, so we can specify the plural name or path with an annotation. For our EmployeeRepository, we make use of this new annotation here called @RepositoryRestResource and then we give a path, so in this example, I have path = members, so then, we'll have the actual endpoint URL of localhost:8080/members. So instead of using /employees, it'll now make use of /members.

Pagination
Page is 0 ZERO based
spring.data.rest.default-page-size=50

Sorting
03:20/10:17

Change the REST resource path
{TabloIsmi}Repository sinifina
@RepositoryRestResource(path="members") --> EKLIYORUZ!!!
public interface TabloIsmiRepository extends JpaRepository<{ENTITY_TYPE}, {PRIMARY_KEY_TYPE}> {}
localhost:8080/employees --> localhost:8080/members
Ilgili degisiklikten sonra localhost:8080/employees GECERSIZDIR. localhost:8080/employees query yapilmaya kalkilirsa 404 Not Found hatasi alinir

Change the Default Page Size
spring.data.rest.default-page-size=3
Page numbers are ZERO BASED

Sort by Last Name
defaultu ascending/artan
urlnin sonuna
?sort=lastname
ekleyip gonderiyoruz POST

Sort by Last Name Descending/azalan
urlnin sonuna
?sort=lastname,desc
ekleyip gonderiyoruz


Thymeleaf pronunciation tymeleaf