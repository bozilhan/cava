PACKAGE
package structure is like the file structure on your computer
package help you to ORGANIZE different files of your program
choose package names related to your specific business topic rather than just using technical terms

CLASS
the name of in java should be a noun
class should be focused ONLY ONE TOPIC
keep your classes AS SMALL AS POSSIBLE to IMPROVE REUSABILITY as well as READABILITY

METHOD
the name of the method should be a VERB that best describes the purpose of the method
keep your methods AS SHORT AS POSSIBLE
MAX 20 LINES
MAX 3-5 PARAMETERS

VARIABLE
a PLACEHOLDER for value that you can set
instance variable, static variable, local variable, parameter, argument

PUBLIC
public is an access modifier that defines any class from any package may use to class or method with public access modifier

VOID
COMMAND methods that CHANCE THE CURRENT STATE and do NOT return a value -> VOID
the query method that returns something should NOT CHANGE THE CURRENT STATE

OBJECT
objects are set of values that exist in computer's memory. Objects' values might change while the program is running

OBJECT ORIENTED LANGUAGE
java is object oriented language. everything is focused around objects in java.
in java writing classes is way of defining a model which focuses on necessary aspects to solve a specific problem


