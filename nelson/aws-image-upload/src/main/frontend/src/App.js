import React from 'react';
import './App.css';
import axios from 'axios';
import {useDropzone} from 'react-dropzone';

/*
* ComponentDidMount
* */
const UserProfiles = () => {
    const [userProfiles, setUserProfiles] = React.useState([]);

    const fetchUserProfiles = () => {
        axios.get('http://localhost:8080/api/v1/user-profile')
            .then(response => {
                console.log(response);
                setUserProfiles(response.data);
            });
    };

    React.useEffect(() => {
        fetchUserProfiles();
    }, []);

    return userProfiles.map((userProfile, index) => {
        return (<div key={index}>
            {userProfile.userProfileId ? (<img
                src={`http://localhost:8080/api/v1/user-profile/${userProfile.userProfileId}/image/download`}/>) : null}
            <br/>
            <br/>
            <h1>{userProfile.username}</h1>
            <p>{userProfile.userProfileId}</p>
            <Dropzone {...userProfile}/>
            <br/>
        </div>);
    });
};

function Dropzone({userProfileId}) {
    const onDrop = React.useCallback(acceptedFiles => {
        // Do something with the files
        const file = acceptedFiles[0];
        console.log(file);

        const formData = new FormData();
        /*
        * backend UserProfileController @RequestParam("file")
        * ile AYNI ISIM!!!
        *
        * this name file has to be the SAME AS backend UserProfileController @RequestParam("file")
        * */
        formData.append('file', file);

        axios.post(`http://localhost:8080/api/v1/user-profile/${userProfileId}/image/upload`, formData,
            {
                headers: {
                    // backend --> consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
                    'Content-Type': 'multipart/form-data'
                }
            }
        ).then(() => console.log('File uploaded successfully'))
            .catch(err => console.log(err));
    }, []);

    const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop});

    return (
        <div {...getRootProps()}>
            <input {...getInputProps()} />
            {
                isDragActive ?
                    <p>Drop the image here ...</p> :
                    <p>Drag 'n' drop profile image, or click to select profile
                        image</p>
            }
        </div>
    );
}

function App() {
    return (
        <div className="App">
            <UserProfiles/>
        </div>
    );
}

export default App;
