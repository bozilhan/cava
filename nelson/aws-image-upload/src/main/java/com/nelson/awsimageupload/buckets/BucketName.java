package com.nelson.awsimageupload.buckets;

public enum BucketName {
    PROFILE_IMAGE("bozilhan-image-upload");

    private final String bucketName;

    BucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getBucketName() {
        return bucketName;
    }
}
