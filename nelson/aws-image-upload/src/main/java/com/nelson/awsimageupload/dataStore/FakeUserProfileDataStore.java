package com.nelson.awsimageupload.dataStore;

import com.nelson.awsimageupload.profile.UserProfile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class FakeUserProfileDataStore {
    private static final List<UserProfile> USER_PROFILES = new ArrayList<>();

    static {
        USER_PROFILES.add(new UserProfile(UUID.fromString("54f8a76a-85a6-4394-8d82-511074f8b274"), "janetjones",
                                          null));
        USER_PROFILES.add(new UserProfile(UUID.fromString("2ce2c4b1-41f2-4a79-87c5-60643e18cc30"), "antoniojunior",
                                          null));
    }

    public List<UserProfile> getUserProfiles() {
        return USER_PROFILES;
    }
}
