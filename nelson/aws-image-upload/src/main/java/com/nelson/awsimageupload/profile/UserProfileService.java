package com.nelson.awsimageupload.profile;

import com.nelson.awsimageupload.buckets.BucketName;
import com.nelson.awsimageupload.filestore.FileStore;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class UserProfileService {
    private final UserProfileDataAccessService userProfileDataAccessService;
    private final FileStore fileStore;

    @Autowired
    public UserProfileService(UserProfileDataAccessService userProfileDataAccessService, FileStore fileStore) {
        this.userProfileDataAccessService = userProfileDataAccessService;
        this.fileStore = fileStore;
    }

    List<UserProfile> getUserProfiles() {
        return userProfileDataAccessService.getUserProfiles();
    }

    void uploadUserProfileImage(UUID userProfileId, MultipartFile file) {
        // 1. check if image is not empty
        isFileEmpty(file);

        // 2. if file is an image
        isImage(file);

        // 3. the user exists in our database
        final UserProfile user = getUserProfileOrThrow(userProfileId);

        // 4. grab some metadata form file if any
        final Map<String, String> metadata = extractMetadata(file);

        // 5. store the image in s3 and update DB (userProfileImageLink) with s3 image link
        final String path = String.format("%s/%s", BucketName.PROFILE_IMAGE.getBucketName(),
                                          user.getUserProfileId());

        final String fileName = String.format("%s-%s", file.getOriginalFilename(),
                                              UUID.randomUUID());

        try {
            fileStore.save(path, fileName, Optional.of(metadata),
                           file.getInputStream());
            user.setUserProfileImageLink(fileName);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public byte[] downloadUserProfileImage(UUID userProfileId) {
        final UserProfile user = getUserProfileOrThrow(userProfileId);
        final String path = String.format("%s/%s", BucketName.PROFILE_IMAGE.getBucketName(),
                                          user.getUserProfileId());

        return user.getUserProfileImageLink()
                .map(key -> fileStore.download(path, key))
                .orElse(new byte[0]);
    }

    private Map<String, String> extractMetadata(MultipartFile file) {
        final Map<String, String> metadata = new HashMap<>();
        metadata.put("Content-type", file.getContentType());
        metadata.put("Content-length", String.valueOf(file.getSize()));
        return metadata;
    }

    private UserProfile getUserProfileOrThrow(UUID userProfileId) {
        return userProfileDataAccessService
                .getUserProfiles()
                .stream()
                .filter(userProfile -> userProfile.getUserProfileId().equals(
                        userProfileId))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(
                        String.format("User profile %s not found",
                                      userProfileId)));
    }

    private void isImage(MultipartFile file) {
        if (!Arrays.asList(ContentType.IMAGE_JPEG.getMimeType(),
                           ContentType.IMAGE_PNG.getMimeType(),
                           ContentType.IMAGE_GIF.getMimeType()).contains(
                file.getContentType())) {
            throw new IllegalStateException(
                    "File must be an image [" + file.getContentType() + "]");
        }
    }

    private void isFileEmpty(MultipartFile file) {
        if (file.isEmpty()) {
            throw new IllegalStateException(
                    "Cannot upload empty file [" + file.getSize() + "]");
        }
    }
}
