/*
 * package keyword tells us that
 * we are INSIDE OF com.nelson DIRECTORY/FOLDER
 * */
package com.nelson.packagesandimport;

/*
 * Says from this package java.util GIVES ME Date CLASS
 * when you want to BRING CLASSES that belong to DIFFERENT PACKAGES
 * */

import java.time.LocalDate;

public class PackagesAndImport {
    /*
     * main entry point to execute your program
     * */
    public static void main(String[] args) {
        Person alex = new Person("alex");

        /*
         * Both alex and mariam are POINTING TO THE SAME INITIAL OBJECT
         * */
        Person mariam = alex;

        System.out.println("before changing alex");

        /*
        alex alex
        * */
        System.out.println(alex.name + " " + mariam.name);

        alex.name = "Alexander";

        System.out.println("after changing alex");

        /*
        Alexander Alexander
        because Both alex and mariam are POINTING TO THE SAME REFERENCE!!!
        */
        System.out.println(alex.name + " " + mariam.name);

        LocalDate localDate = LocalDate.now();

        /*
        * ALTERNATIVE
        * WITHOUT IMPORTING PACKAGES
        * IMPORT EXPLICITLY
        * This usage dont use in best practice
        * we want all the import statements AT THE VERY  TOP
        *
        * When you have TWO CLASSES whose names are the same but exist in different packages
        * */
        java.util.Date date = new java.util.Date();
        java.time.LocalDateTime localDateTime =  java.time.LocalDateTime.now();
    }

    static class Person {
        String name;

        Person(String name) {
            this.name = name;
        }
    }
}
