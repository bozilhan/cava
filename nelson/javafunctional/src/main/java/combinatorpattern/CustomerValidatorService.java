package combinatorpattern;

import java.time.LocalDate;
import java.time.Period;

/*
 * we have a system where customers do register to our system
 * and we need to perform some kind of VALIDATION
 * */
public class CustomerValidatorService {
    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPhoneNumberValid(String phoneNumber) {
        return phoneNumber.startsWith("+0");
    }

    private boolean isAdult(LocalDate dob) {
        return Period.between(dob, LocalDate.now()).getYears() > 16;
    }

    /*
     * Combine everything
     * We dont know EXACTLY you know what is wrong with our validation
     *
     * Combinator pattern allows you to CHANGE FUNCTIONS TOGETHER
     * Basically combinator is a function that might TAKE OTHER FUNCTION AS ARGUMENTS
     * and RETURN NEW FUNCTIONS
     *
     * */
    public boolean isValid(Customer customer) {
        return isEmailValid(customer.getEmail()) &&
                isPhoneNumberValid(customer.getPhoneNumber()) &&
                isAdult(customer.getDob());
    }
}
