package combinatorpattern;

import java.time.LocalDate;

public class MyCombinatorPattern {
    public static void main(String[] args) {
        final Customer alice = new Customer(
                "Alice",
                "alicegmail.com",
                "20934893",
                LocalDate.of(2010, 1, 1));

        // System.out.println(new CustomerValidatorService().isValid(alice));

        /*
         * if valid we can store customer in db
         * using COMBINATOR PATTERN
         * */
        final CustomerRegistrationValidator.ValidationResult result = CustomerRegistrationValidator
                .isEmailValid()
                .and(CustomerRegistrationValidator.isPhoneNumberValid())
                .and(CustomerRegistrationValidator.isAnAdult())
                .apply(alice);

        if (result != CustomerRegistrationValidator.ValidationResult.SUCCESS) {
            throw new IllegalStateException(result.name());
        }
    }
}
