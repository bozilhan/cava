package declarative;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MyDeclarative {
    public static void main(String[] args) {
        final List<Person> people = List.of(
                new Person("Barz", Gender.MALE),
                new Person("Never", Gender.FEMALE),
                new Person("Give", Gender.FEMALE),
                new Person("Up", Gender.FEMALE)
        );

        /*
         * Declarative Approach --> by using streams with collections
         *
         * Stream allows us to go into an abstract mode
         * where we simply tell it what we want
         *
         * we are not creating list and then adding to that list and then
         * doing the if statement so on and so forth
         * */
        people.stream()
                .filter(person -> Gender.FEMALE.equals(person.gender))
                .forEach(System.out::println);

        final List<Person> females = people.stream()
                .filter(person -> Gender.FEMALE.equals(person.gender))
                .collect(Collectors.toList());

        females.forEach(System.out::println);

        /*
         * functionalinterface.predicate accepts a person and than return true or false BASED OF THAT person
         * */
        final Predicate<Person> personPredicate = person -> Gender.FEMALE.equals(person.gender);

    }

    static class Person {
        private final String name;
        private final Gender gender;

        Person(String name, Gender gender) {
            this.name = name;
            this.gender = gender;
        }
    }

    enum Gender {
        MALE, FEMALE
    }
}
