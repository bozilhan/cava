package functionalinterfaces.biconsumerfunctionalinterface;

import java.util.function.BiConsumer;

public class MyBiConsumer {
    public static void main(String[] args) {
        final Customer maria = new Customer("Maria", "9999");

        greetCustomerBiConsumer.accept(maria, false);
    }

    /*
     * Consumer<T,U> represents an operation that accepts TWO INPUT arguments and
     * returns NO RESULT
     *
     * it is like a void function
     *
     * inputs and outputs of functions have to be OBJECT TYPE NOT the PRIMITIVE
     *
     * we can ONLY use static methods INSIDE main()
     * */
    static BiConsumer<Customer, Boolean> greetCustomerBiConsumer = (customer, showPhoneNumber) ->
            System.out.println("Hello " + customer.customerName +
                                       ", thanks for registering phone " +
                                       (showPhoneNumber ? customer.customerPhoneNumber : "******"));

    static class Customer {
        private final String customerName;
        private final String customerPhoneNumber;

        public Customer(String customerName, String customerPhoneNumber) {
            this.customerName = customerName;
            this.customerPhoneNumber = customerPhoneNumber;
        }
    }
}
