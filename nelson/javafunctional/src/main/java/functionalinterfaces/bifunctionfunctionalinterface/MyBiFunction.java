package functionalinterfaces.bifunctionfunctionalinterface;

import java.util.function.BiFunction;

public class MyBiFunction {
    public static void main(String[] args) {
        /*
         * (4+1)*100 --> 500
         * */
        final Integer result = incrementByOneAndMultiplyByTenBiFunction.apply(4, 100);
    }

    /*
     * BiFunction is the same as a Function<T,R>
     * but instead of one input BiFunction takes TWO INPUTS and
     * produces one output
     *
     * the 1st argument is the first input
     *
     * inputs and outputs of functions have to be OBJECT TYPE NOT the PRIMITIVE
     */
    static BiFunction<Integer, Integer, Integer> incrementByOneAndMultiplyByTenBiFunction =
            (numberToIncrementByOne, numberToMultiplyBy) -> (numberToIncrementByOne + 1) * numberToMultiplyBy;

    static int incrementByOneAndMultiplyByTen(int numToAddBy, int numToMultiplyBy) {
        return (numToAddBy + 1) * numToMultiplyBy;
    }
}
