package functionalinterfaces.chainingfunctions;

import java.util.function.Function;

public class MyChainingFunctions {
    public static void main(String[] args) {
        int increment2 = incrementByOneFunction.apply(1);

        /*
         * soldan saga okunuyor. Ilk metodun sonucu ikinci metodun inputu oluyor.
         * apply(1) oncelikle incrementByOneFunction i 1 ile cagirip sureci baslatiyor
         * (1+1)*10 --> 20
         * 1 yerine 4 yazssak (4+1)*10 --> 50
         */
        final Integer addByOneAndThenMultiplyByTen = incrementByOneFunction.andThen(multiplyByTenFunction).apply(1);
    }

    /*
     * Represents a function that accepts ONE ARGUMENT and produces a result
     * Function<T, R>
     * T --> Data type of the input
     * R --> Data type of the output
     *
     * inputs and outputs of functions have to be OBJECT TYPE NOT the PRIMITIVE
     */
    static Function<Integer, Integer> incrementByOneFunction = number -> number + 1;

    static Function<Integer, Integer> multiplyByTenFunction = number -> number * 10;
}
