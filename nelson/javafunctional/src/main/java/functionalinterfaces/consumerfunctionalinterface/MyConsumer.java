package functionalinterfaces.consumerfunctionalinterface;

import java.util.function.Consumer;

public class MyConsumer {
    public static void main(String[] args) {
        final Customer maria = new Customer("Maria", "9999");

        greetCustomer(maria);
        greetCustomerConsumer.accept(maria);
    }

    /*
     * Consumer<T> represents an operation that accepts A SINGLE INPUT argument and
     * returns NO RESULT
     *
     * it is like a void function
     *
     * inputs and outputs of functions have to be OBJECT TYPE NOT the PRIMITIVE
     * */
    static Consumer<Customer> greetCustomerConsumer = customer ->
            System.out.println("Hello " + customer.customerName +
                                       ", thanks for registering phone " +
                                       customer.customerPhoneNumber);

    static void greetCustomer(Customer customer) {
        System.out.println("Hello " + customer.customerName +
                                   ", thanks for registering phone " +
                                   customer.customerPhoneNumber);
    }

    static class Customer {
        private final String customerName;
        private final String customerPhoneNumber;

        public Customer(String customerName, String customerPhoneNumber) {
            this.customerName = customerName;
            this.customerPhoneNumber = customerPhoneNumber;
        }
    }

}
