package functionalinterfaces.function;

import java.util.function.Function;

/*
* the benefit of functional interfaces is
* when you start to use OPTIONALS and STREAMS
* but more specific STREAMS
*
* once you start using STREAMS with all of functional interfaces
* (Bi)Function, (Bi)Supplier, (Bi)Predicate, (Bi)Consumer
* you will start to write clean code
*
* */
public class MyFunction {
    public static void main(String[] args) {
        int increment = increment(0);

        int increment2 = incrementByOneFunction.apply(1);
    }

    /*
     * Represents a function that accepts ONE ARGUMENT and produces a result
     * Function<T, R>
     * T --> Data type of the input
     * R --> Data type of the output
     *
     * inputs and outputs of functions have to be OBJECT TYPE NOT the PRIMITIVE
     */
    static Function<Integer, Integer> incrementByOneFunction = number -> number + 1;

    static int increment(int number) {
        return number + 1;
    }
}
