package functionalinterfaces.predicate;

import java.util.function.Predicate;

public class MyPredicate {
    public static void main(String[] args) {
        isPhoneNumberValidPredicate.test("09009877300");

        /*
        * soldan saga baslaniyor.
        * baslangicta test() metodunun argumani en soldaki metodu tetikliyor.
        * ilk metod true donerse test() metodunun argumani
        * */
        isPhoneNumberValidPredicate.and(containsNumber3).test("09009877300");

        isPhoneNumberValidPredicate.or(containsNumber3).test("09009877300");
    }

    /*
     * functionalinterface.predicate(dogrulamak)
     *
     * you can have a bunch of predicates and then COMBINE them TOGETHER
     *
     * BiPredicate<T,U> da mevcut
     * Predicate<T> --> type of the argument
     * Predicate<String> --> type of the phoneNumber
     *
     * inputs and outputs of functions have to be OBJECT TYPE NOT the PRIMITIVE
     * */
    static Predicate<String> isPhoneNumberValidPredicate =
            phoneNumber -> phoneNumber.startsWith("07") && phoneNumber.length() == 11;

    static Predicate<String> containsNumber3 = phoneNumber -> phoneNumber.contains("3");

    static boolean isPhoneNumberValid(String phoneNumber) {
        return phoneNumber.startsWith("07") && phoneNumber.length() == 11;
    }
}
