package functionalinterfaces.supplier;

import java.util.List;
import java.util.function.Supplier;

public class MySupplier {
    public static void main(String[] args) {

        /*
        * [jdbc://localhost:5432/users, jdbc://localhost:5432/customer]
        * */
        getDBConnectionUrlsSupplier.get();
    }

    /*
     * Supplier(tedarikci, ihtiyaci karsilayan)
     * Supplier<T> represents a functionalinterface.supplier of results
     *
     * Supplier returns ANY KIND OF VALUE that you want
     * Custom classes, array, list, custom object, String, Integer etc.
     *
     * inputs and outputs of functions have to be OBJECT TYPE NOT the PRIMITIVE
     * */
    static Supplier<List<String>> getDBConnectionUrlsSupplier = () -> List.of("jdbc://localhost:5432/users","jdbc://localhost:5432/customer");

    /*
     * the purpose of this function is simply to return a value
     * */
    static String getDBConnectionUrl() {
        return "jdbc://localhost:5432/users";
    }
}
