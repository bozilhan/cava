package imperative;


import java.util.ArrayList;
import java.util.List;

/*
 * imperative programming
 * imperative programming involves you as a developer DEFINING EVERY SINGLE DETAIL ABOUT YOUR IMPLEMENTATION.
 * we are defining EVERY SINGLE DETAIL
 * 1. we have empty list
 * 2. and then we loop through that
 * 3. and then we have a if statement
 * 4. finally we have final for loop looping through females
 *
 * This is A LOT OF CODE for something VERY SIMPLE
 *
 * we can ONLY use static methods INSIDE main()
 */
public class MyImperative {
    public static void main(String[] args) {
        final List<Person> people = List.of(
                new Person("Barz", Gender.MALE),
                new Person("Never", Gender.FEMALE),
                new Person("Give", Gender.FEMALE),
                new Person("Up", Gender.FEMALE)
        );

        final List<Person> females = new ArrayList<>();

        for (final Person person : people) {
            if (Gender.FEMALE.equals(person.gender)) {
                females.add(person);
            }
        }

        for (final Person female : females) {
            System.out.println(female);
        }
    }

    static class Person {
        private final String name;
        private final Gender gender;

        Person(String name, Gender gender) {
            this.name = name;
            this.gender = gender;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", gender=" + gender +
                    '}';
        }
    }

    enum Gender {
        MALE, FEMALE
    }
}
