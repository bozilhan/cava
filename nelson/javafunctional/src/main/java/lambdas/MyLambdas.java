package lambdas;

import java.util.function.BiFunction;
import java.util.function.Function;

public class MyLambdas {
    public static void main(String[] args) {
        Function<String, String> upperCaseName = name -> name.toUpperCase();
        Function<String, String> upperCaseNameExactSameThing = String::toUpperCase;

        /*
         * if you have to do some extra logic and NOT return IMMEDIATELY
         * you need to have {} curly braces
         * */
        BiFunction<String, Integer, String> extraLogic = (name, age) -> {
            // logic

            if (name.isBlank()) {
                throw new IllegalArgumentException("");
            }
            System.out.println(age);
            return name.toUpperCase();
        };

        extraLogic.apply("alex", 20);
    }
}
