package optionals;

import java.util.Optional;

/*
 * Optionals changes the way
 * that you deal with NULL POINTER EXCEPTIONS
 * */
public class MyOptionals {
    public static void main(String[] args) {
        /*
         * value --> default value
         * */
        final Object value = Optional.ofNullable(null)
                .orElseGet(() -> "default value");

        /*
         * value2 --> Hello
         * */
        final Object value2 = Optional.ofNullable("Hello")
                .orElseGet(() -> "default value");

        final Object value3 = Optional.ofNullable("Hello")
                .orElseThrow(() -> new IllegalStateException("exception"));

        /*
         * ifPresent we will receive the actual value here and then
         * we do something with the value
         *
         * "john@gmail.com" yerine null yazarsak consola herhangi bir sey BASILMAZ!!!
         * */
        Optional.ofNullable("john@gmail.com")
                .ifPresent(email -> System.out.println("sending email to " + email));

        /*
         * consolea cannot send email" basilir.
         * */
        Optional.ofNullable("john@gmail.com")
                .ifPresentOrElse(email -> System.out.println("sending email to " + email),
                                 () -> System.out.println("cannot send email"));
     }
}
