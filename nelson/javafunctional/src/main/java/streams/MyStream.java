package streams;

import java.util.List;
import java.util.function.Function;
import java.util.function.IntConsumer;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

/*
 * java streams API takes us to abstraction mode
 */
public class MyStream {
    public static void main(String[] args) {
        final List<Person> people = List.of(
                new Person("Barz", Gender.MALE),
                new Person("Never", Gender.FEMALE),
                new Person("Give", Gender.FEMALE),
                new Person("Up", Gender.FEMALE)
        );


        /*
         * map() function simply does TRANSFORMATIONS(donusum, donusturme, sekil degistirme)
         *
         * lambda method reference --> System.out::println
         * .forEach(gender -> System.out.println(gender)) === .forEach(System.out::println)
         * .mapToInt(name->name.length()) === .mapToInt(String::length)
         *
         * output : MALE\nFEMALE
         */
        people.stream()
                .map(person -> person.gender)
                .collect(Collectors.toSet())
                .forEach(System.out::println);


        /*
         * Function that takes a Person returns a String
         * */
        final Function<Person, String> personStringFunction = person -> person.name;

        /*
         * ToIntFunction takes a String and returns an Integer
         * */
        final ToIntFunction<String> length = String::length;

        /*
         * IntConsumer simply takes in Integer and prints out the result
         * */
        final IntConsumer println = System.out::println;

        people.stream()
                .map(personStringFunction)
                .mapToInt(length)
                .forEach(println);

        /*
         * returns false because our list contains both males and females
         * */
        final Predicate<Person> personPredicate = person -> Gender.FEMALE.equals(person.gender);
        final boolean containsOnlyFemales = people.stream()
                .allMatch(personPredicate);
    }

    static class Person {
        private final String name;
        private final Gender gender;

        Person(String name, Gender gender) {
            this.name = name;
            this.gender = gender;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", gender=" + gender +
                    '}';
        }
    }

    enum Gender {
        MALE, FEMALE;
    }
}
