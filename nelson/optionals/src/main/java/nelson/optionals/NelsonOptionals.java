package nelson.optionals;

import java.util.Optional;

public class NelsonOptionals {
    public static void main(String[] args) {
        Optional<Object> optionalEmpty = Optional.empty();
        System.out.println(optionalEmpty);
        System.out.println(optionalEmpty.isPresent());
        System.out.println(optionalEmpty.isEmpty());

        Optional<String> optionalOf = Optional.of("hello");
        System.out.println(optionalOf.isPresent());
        System.out.println(optionalOf.isEmpty());


        /*
         * default value when the value inside of the Optional is NOT PRESENT
         * */
        String optionalOfOrElse = optionalOf
              /*
               * map() allows me to do a TRANSFORMATION on the actual value inside of the
               * */
              .map(String::toUpperCase)
              .orElse("hello world");

        String optionalOfOrElseGet = optionalOf
              .map(String::toUpperCase)
              .orElseGet(() -> {
                  /*
                   * inside of this supplier we can have any logic
                   * to get a default value
                   * */

                  /*
                   * ... extra computation to retrieve the value
                   * */

                  /*
                   * return the value that you have computed
                   *
                   * you could imagine that `kjh` for example was in the database
                   * so inside here you could actually say
                   * you know go to the database and then fetch the `kjh` part
                   * in case `hello` is not there
                   * */
                  return "kjh";
              });

        /*
         * when you are not sure whether the value will be nullable or not
         * you can use Optional.ofNullable
         */
        Optional<Object> optionalOfNullable = Optional.ofNullable("asdf");

        optionalOfNullable.ifPresent(System.out::println);

        optionalOfNullable.ifPresentOrElse(System.out::println, () -> System.out.println("RnnBLE"));

        Person personJames = new Person("james", "JAMES@gmail.com");
        System.out.println(personJames.getEmail().toLowerCase());

        Person personJamesEmailNull = new Person("james", null);

        /*
         * throws NullPointerException
         * */
        System.out.println(personJamesEmailNull.getEmail().toLowerCase());

        /*
         * 1st alternative
         * STEP2
         * */
        System.out.println(
              personJamesEmailNull.getEmailOptional()
                    .map(String::toLowerCase)
                    .orElse("email not provided"));


        /*
         * 2nd alternative
         * */
        if (personJamesEmailNull.getEmailOptional().isPresent()) {
            /*
             * getting the value inside of the optional
             * */
            String email = personJamesEmailNull.getEmailOptional().get();
        } else {
            // "email not provided"
        }
    }
}

class Person {
    private final String name;
    private final String email;

    public Person(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    /*
     * 1st alternative
     * STEP1
     * */
    public Optional<String> getEmailOptional() {
        return Optional.ofNullable(email);
    }
}
