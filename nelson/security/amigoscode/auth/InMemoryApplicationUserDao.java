package com.bozilhan.security.amigoscode.auth;

import com.bozilhan.security.amigoscode.security.ApplicationUserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository("InMemory")
public class InMemoryApplicationUserDao implements ApplicationUserDao {
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public InMemoryApplicationUserDao(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Optional<ApplicationUser> selectApplicationUserByUsername(String username) {
        return getApplicationUsers()
                .stream()
                .filter(applicationUser -> username.equals(applicationUser.getUsername()))
                .findFirst();
    }

    private List<ApplicationUser> getApplicationUsers() {

        List<ApplicationUser> applicationUsers = Stream.of(
                new ApplicationUser("annasmith",
                                    passwordEncoder.encode("password"),
                                    ApplicationUserRole.STUDENT.getGrantedAuthorities(),
                                    true,
                                    true,
                                    true,
                                    true),
                new ApplicationUser("linda",
                                    passwordEncoder.encode("password"),
                                    ApplicationUserRole.ADMIN.getGrantedAuthorities(),
                                    true,
                                    true,
                                    true,
                                    true),
                new ApplicationUser("tom",
                                    passwordEncoder.encode("password"),
                                    ApplicationUserRole.ADMINTRAINEE.getGrantedAuthorities(),
                                    true,
                                    true,
                                    true,
                                    true))
                .collect(Collectors.toList());

        return applicationUsers;
    }
}
