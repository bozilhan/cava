package com.bozilhan.security.amigoscode.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.crypto.SecretKey;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class JwtTokenVerifier extends OncePerRequestFilter {
    private final SecretKey secretKey;
    private final JwtConfig jwtConfig;

    public JwtTokenVerifier(SecretKey secretKey, JwtConfig jwtConfig) {
        this.secretKey = secretKey;
        this.jwtConfig = jwtConfig;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {

        /*
         * 1. get the token from the header
         * when the client submits the request the client will send a token along
         * */
        String authorizationHeader = request.getHeader(jwtConfig.getAuthorizationHeader());

        if (Optional.ofNullable(authorizationHeader).orElse("").trim().isEmpty() || !authorizationHeader.startsWith(
                jwtConfig.getTokenPrefix())) {
            /*
             * The request will be rejected
             * */
            filterChain.doFilter(request, response);
            return;
        }

        /*
         * Grab token itself without "Bearer "
         * */
        String token = authorizationHeader.replace(jwtConfig.getTokenPrefix(), "");

        try {
            Claims claimsJwsBody = Jwts.parserBuilder()
                    .setSigningKey(secretKey)
                    .build()
                    .parseClaimsJwt(token).getBody();

            String username = claimsJwsBody.getSubject();

            List<Map<String, String>> authorities = (List<Map<String, String>>) claimsJwsBody.get("authorities");

            Set<SimpleGrantedAuthority> simpleGrantedAuthorities = authorities.stream()
                    .map(m -> new SimpleGrantedAuthority(m.get("authority")))
                    .collect(Collectors.toSet());

            /*
             * tell spring security this user can be authenticated
             * */
            Object principal;
            Object credentials;
            Authentication authentication = new UsernamePasswordAuthenticationToken(username,
                                                                                    null,
                                                                                    simpleGrantedAuthorities);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (JwtException e) {
            /*
             * The token is invalid
             * it may be modified or expired
             * */
            throw new IllegalStateException(String.format("Token %s cannot be trusted", token));
        }

        filterChain.doFilter(request, response);
    }
}
