package com.nelson.sprinboot.dao;

import com.nelson.sprinboot.model.Person;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/*
 * we are going to define the operations allowed or
 * the actual contract for anyone that wishes to
 * implement this interface
 * */
public interface PersonDao {
    int insertPerson(UUID id, Person person);

    default int insertPerson(Person person) {
        UUID id = UUID.randomUUID();
        return insertPerson(id, person);
    }

    Optional<Person> selectPersonById(UUID id);

    List<Person> getAllPeople();

    int deletePersonById(UUID id);

    int updatePersonById(UUID id, Person person);
}
