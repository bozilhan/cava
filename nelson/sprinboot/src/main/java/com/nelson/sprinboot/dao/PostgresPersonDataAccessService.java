package com.nelson.sprinboot.dao;

import com.nelson.sprinboot.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("postgres")
public class PostgresPersonDataAccessService implements PersonDao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PostgresPersonDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertPerson(UUID id, Person person) {
        return 0;
    }

    @Override
    public Optional<Person> selectPersonById(UUID id) {
        final String sql = "SELECT id, name FROM person WHERE id = ?";

        final Person person = jdbcTemplate.queryForObject(sql, new Object[]{id}, (resultSet, i) -> {
            UUID personId = UUID.fromString(resultSet.getString("id"));
            String name = resultSet.getString("name");
            return new Person(personId, name);
        });

        return Optional.ofNullable(person);
    }

    @Override
    public List<Person> getAllPeople() {
        /*
         * we prefer to use the actual column names INSTEAD OF *
         * */
        final String sql = "SELECT id, name FROM person";

        /*
         * query returns LIST
         *
         * query takes two things
         * we can pass sql statement and then we have to pass RowMapper
         * basically row mapper is how we take the values
         * that we will retrieve from our DB and then
         * TRANSFORM that into JAVA OBJECT
         *
         * RowMapper is lambda that has access to the result set as well as the index
         * */
        final List<Person> listOfPeople = jdbcTemplate.query(sql, (resultSet, i) -> {
            UUID id = UUID.fromString(resultSet.getString("id"));
            String name = resultSet.getString("name");

            return new Person(id, name);
        });

        return listOfPeople;
    }

    @Override
    public int deletePersonById(UUID id) {
        return 0;
    }

    @Override
    public int updatePersonById(UUID id, Person person) {
        return 0;
    }
}
