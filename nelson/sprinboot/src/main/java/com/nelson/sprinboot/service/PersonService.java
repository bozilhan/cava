package com.nelson.sprinboot.service;

import com.nelson.sprinboot.dao.PersonDao;
import com.nelson.sprinboot.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/*
 * You have whatever logic
 * your application REQUIRES
 * */
@Service
public class PersonService {
    /*
     * make SURE you get the ACTUAL INTERFACE and
     * NOT the CONCRETE CLASS
     * */
    private final PersonDao personDao;

    @Autowired
    public PersonService(@Qualifier("fakeDao") PersonDao personDao) {
    /*  public PersonService(@Qualifier("mongo") PersonDao personDao) {
        public PersonService(@Qualifier("mySQL") PersonDao personDao) {
        public PersonService(@Qualifier("postgres") PersonDao personDao) {
        ...... SADECE isim degisiyor
        ...... SADECE 1 satir degisiyor
    */
        this.personDao = personDao;
    }

    public int insertPerson(Person person) {
        return personDao.insertPerson(person);
    }

    public List<Person> getAllPeople() {
        return personDao.getAllPeople();
    }

    public Optional<Person> getPersonById(UUID id) {
        return personDao.selectPersonById(id);
    }

    public int deletePerson(UUID id) {
        return personDao.deletePersonById(id);
    }

    public int updatePerson(UUID id, Person newPerson) {
        return personDao.updatePersonById(id, newPerson);
    }
}
