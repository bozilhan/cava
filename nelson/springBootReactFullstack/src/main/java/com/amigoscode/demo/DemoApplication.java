package com.amigoscode.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);

        printMessage("Hello world");
    }

    private static void printMessage(String helloWorld) {
        System.out.println(helloWorld);
        System.out.println(helloWorld);
    }
}
