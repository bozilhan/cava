package com.nelson.springdatajpa;

import com.nelson.springdatajpa.models.Student;
import com.nelson.springdatajpa.repository.StudentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /*
     * run some code after the application startup
     * */
    @Bean
    CommandLineRunner commandLineRunner(StudentRepository studentRepository) {
        return args -> {
            Student maria = new Student("maria", "jones", "mj@aaa.com", 21);
            studentRepository.save(maria);
        };
    }
}
