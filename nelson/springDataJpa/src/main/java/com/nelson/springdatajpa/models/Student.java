package com.nelson.springdatajpa.models;

import javax.persistence.*;

/*
 * this is a class that will represent our ENTITY in our database
 *
 * tell spring data jpa that we want this class we have just created
 * we want it to be a table and also we want these attributes to be a column in our table
 *
 *
 * to map this class to a table we need to use @Entity annotation
 *
 *
 * when you have your entities it's ALWAYS GOOD PRACTICE to ADD a NAME
 * the DEFAULT is the ACTUAL CLASS NAME
 * */
@Entity(name = "Student")
@Table(
      name = "student",
      uniqueConstraints = {
            @UniqueConstraint(
                  name = "student_email_unique",
                  /*
                  * we can also pass an array of the columns
                  * */
                  columnNames = "email")
      }
)
public class Student {

    @Id
    @SequenceGenerator(
          name = "student_sequence",
          sequenceName = "student_sequence",
          /*
          * how much will the sequence increase from
          * we want to go from 1, 2, 3, 4, 5, ...
          * */
          allocationSize = 1
    )
    @GeneratedValue(
          strategy = GenerationType.SEQUENCE,
          /*
          * sequenceName of @SequenceGenerator
          * */
          generator = "student_sequence"
    )
    /*
     * to have full control of the name
     * it is good practice to specify the name that we want for our columns
     * we should @Column for every single property that we have
     * */
    @Column(
          name = "id",
          /*
          * we dont want no one to update this column
          * */
          updatable = false
    )
    private Long id;

    @Column(
          name = "first_name",
          nullable = false,
          columnDefinition = "TEXT"
    )
    private String firstName;

    @Column(
          name = "last_name",
          nullable = false,
          columnDefinition = "TEXT"
    )
    private String lastName;

    @Column(
          name = "email",
          nullable = false,
          columnDefinition = "TEXT"
    )
    private String email;


    @Column(name = "age",
          nullable = false)
    private Integer age;

    public Student() {
    }

    public Student(String firstName, String lastName, String email, Integer age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
              "id=" + id +
              ", firstName='" + firstName + '\'' +
              ", lastName='" + lastName + '\'' +
              ", email='" + email + '\'' +
              ", age=" + age +
              '}';
    }
}
