package com.nelson.springdatajpa.repository;

import com.nelson.springdatajpa.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;

/*
* <Student, Long> entity and its identifier data type
* */
public interface StudentRepository extends JpaRepository<Student, Long> {
}
