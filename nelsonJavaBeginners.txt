https://www.youtube.com/watch?v=Qgl81fPcLc8
JDK Java Development Kit
JDK is an environment that gives us everything that we need to develop java applications
inside of the JDK we have

JRE Java Runtime Environment
JRE contains all the libraries and classes for us to develop these applications

JVM Java Virtual Machine
JVM is responsible of executing your java programs

LTS -> Long Term Support
we should learn a version that has LONG TERM SUPPORT -> JDK8, JDK11, JDK17
JDK 15 ->  Sep 2020

char a = 'A' -> single quotes
String a = "Hooray"; -> double quotes

24:28
COMPILING JAVAC AND BYTE CODE
Terminalde ilgili dizine gidip
javac {MY_CLASS}.java -> {MY_CLASS}.class

29:27
RUNNING JAVA PROGRAMS FROM TERMINAL
invoke jvm
src dizini altinda
java {ILK_ADIMDAN_ITIBAREN_PACKAGE}.Main

34:49
PACKAGES
package is simply a way of you organizing your classes together
the package is simply a FOLDER STRUCTURE
package is simply a DIRECTORY STRUCTURE that ALLOWS YOU TO GROUP CLASSES TOGETHER

COMMENTS
comments are useful when you want to DOCUMENT YOUR CODE
You should definitely have  comments when you have a piece of code WHICH IS NOT TRIVIAL FOR THE READER
comments are there to help other developers when they see your code to understand the functionality of what you wrote

VARIABLES
a variable is simply a BOX. just think of variable AS A BOX. Inside of this box you can STORE ONLY AND ONLY ONE THING
if i want to access my variable. Anytime that you want to find out(bulup cikarmak) CONTENTS of this box, the actual value, you call name of the box
when you define a variable you need to specify the actual data type
a variable is a PLACEHOLDER WHERE YOU CAN STORE VALUES
FIRST you need to decide on the DATA TYPE
with java we have 2 options. You have PRIMITIVE (int, long, double, char-single characters-, boolean) data types or REFERENCE (the data type in itself-basli basina- it's more complex than primitives) data types
DATA_TYPE NAME_OF_YOUR_VARIABLE = THE_CONTENT_INSIDE;
    int number = 5;
    double doubleNumber = 100.01;
    String brand = "Barz";

PRIMITIVE DATA TYPES
long theLongWithoutL = 78783833432; -> it should work
long theLong = 78783833432L; -> BEST PRACTICE APPEND 'l' or 'L'
float theFloat = 3.14F 'f' or 'F' -> store 6-7 Decimal digit
double theDouble = 3.1415; -> store 15 decimal digits
boolean isAdult = true;
char theChar = 'A';

underscores is mainly to HELP you VISUALIZE the actual number
long theLong = 78_783_833_432L;

REFERENCE/NON-PRIMITIVE TYPES
reference types ALWAYS START WITH UPPERCASE
// name now is an OBJECT of type String
String name = new String("Barz");
LocalDate now = LocalDate.now(); -> 2020-07-07

DIFFERENCE BETWEEN PRIMITIVES AND REFERENCE TYPES
int a = 10;

// PASSED BY COPY
// which means that if we CHANGE a, b is UNTOUCHED
int b = a;
a = 100 -> a=100, b=10

THE STRING CLASS
Strings in Java are IMMUTABLE.
You can read a char from a specific index with charAt(int index) but you CAN NOT MODIFY IT.
    String word = "schnucks";
    char[] array = word.toCharArray();
    Then you are free to change any letter as you wish. e.g.
    array[4] = 'a';

    To retrieve the modified String, simply use
    word = new String(array);

// nameBarz and nameBarzi now are OBJECTs of type String
String nameBarz = new String("Barz");
String nameBarzi = "Barzi";

nameBarz.equals(nameBarzi) -> false

It is a common case in programming when you want to check if specific String contains a particular substring.
nameBarz.contains(nameBarzi) -> false

CASTING
(int) Math.sqrt(25) -> (int) 5.0 -> 5

SWITCH STATEMENTS
when you are switching or you are performing an if condition ONLY ON ONE VALUE
complex comparison of operators and greater bigger you SHOULDNT really USE SWITCH STATEMENT

break; -> break out of this switch statement

ARRAYS
    // argumman olarak array alan metodu cagirma
    int count = countOccurrences(letters);

    // ilgili metod arrayin STATEINI DEGISTIRIR!!!
    // ARRAY REFERENCE TYPE
    public static void countOccurrences(char[] letters) {
        char temp = letters[0];
        letters[0] = letters[letters.length - 1];
        letters[letters.length - 1] = temp;
    }

int [] numbers = new int[2];
int [] numbers = {2, 0, 1}; -> we dont need to SPECIFY SIZE
Arrays.toString(numbers) -> [0,0]

ENHANCED FOR LOOP
we DONT ACCESS THE INDEX!!!
for(int number : numbers){}
Arrays.stream(numbers).forEach(System.out::println)

BREAK AND CONTINUE
break allows us to do is simply break out(cikmak) of the loop

continue instead of breaking out of the loop continue simply GOES BACK TO THE BEGINNING OF THE LOOP
String [] names = {"Anna", "Ali","Bob","Mike"};
for(String name : names){
    if(name.startsWith("A")) {
        // we are not running anything after this line
        // this line is executed
        // we go back to the beginning of the loop
        // and then RUN EVERYTHING AGAGIN
        continue;
    }
    System.out.println(name); -> Bob Mike
}

WHILE LOOP
while loop allows us to loop while a boolean expression evaluates to true

DO WHILE LOOP
do while loop executes NO MATTER WHAT AT LEAST ONCE

TAKING USER INPUT
SCANNER CLASS
Scanner class allows us to TAKE USER INPUT FROM THE CONSOLE
Scanner scanner = new Scanner(System.in);
System.out.println("What is your name?");
String userName = scanner.nextLine();
int age = scanner.nextInt();
