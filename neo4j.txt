GRAPH DATABASES
relational databases use a ledger-style(defter) structure. Sometimes you split up a thing into its parts and and it's streched across a few tables but generally we have at least some level of entity that's a ROW TABLE and we use FOREIGN KEY CONSTRAINTS and those represent relationships. Relation is kind of an agreement. that is the keyword for relational databases how do you make relationships between stuff you make it BASED ON CONSTRAINTS

relational databases can get complex and rigid when repersenting relationships. we have to perform multi-level joins. multi-level joins are slow and complex

GRAPH DATABASE THINGS ARE NODES OR VERTICES. Nodes are key-value pairs PROPERTY GRAPH MODEL is sort of the most common. Nodes have labels to tell you what type of thing it is (what table that is in). this type is user, person ...

n: person
id: 1234
first_name:"ed"
last_name:"Barz"

NODES ARE CONNEnected by RELATIONSHIPS OR EDGES

relationships have a TYPE and a DIRECTION(bi-directional, unidirectional) and can have PROPERTIES

direct and indirect relationships.
indirect relationship is harder to do traverse with relational databases. we have to perform multiple joins

Graph is designed to be able to traverse. graphs uses math
we can add more relationships as needed and still be performant

NEO4J uses CIPHER/GREMLIN  query language

cipher is basically sql for graphs

What is a graph database? (in 10 minutes)
neo4j is a transactional database it is noSQL category but it is different
neo stores the data in a shape of graph like a network

neo is NATIVE GRAPH DATABASE
traversal time is consistent

hetionet
neo4j sandbox BEST PLACE TO START

graph
internet, water molecule

Intro to Graph Databases Episode #1 - Evolution of DBs
graph databases store data in much more logical fashion. a way that represents the real world and prioritze the representation discoveryability and maintability of data relationships

graph databases are used for dozens of use cases, from fraud(dolandiricilik) detection, to real time recommendations, network and it operations, social NETWORK


properties of graph databases
intuitiveness(sezgisellik)
means a way to create and maintain your data in LOGICAL FASHION(bicim). it means lessening the translation friction not just translation from code into database  calls but also translation between the business folks describing the application requirements and the developers satisfiying those requirements

in graph database the PAPER/WHITE BOARD MODEL is the PHYSICAL MODEL!!!. what we draw on the paper refers to on our harddisk

speed
how quickly we can deliver RESULTS
less code faster time
ebay, wallmart, cisco uses neo4j because of the performance advantages

AGILITY
agility is another measure of speed how easily and quickly can your code adapt to the changing business
NEO4j provides a naturally SCHEMA OPTIONAL data model
NO MORE SCHEMA MIGRATIONS!!!

CYPHER is similar to SQL that is optimized for GRAPHS

CYPHER
less time writing queries
giving more time to understand the answers and leaving time to ask the next question
allows you to spend less time DEBUGGING queries which means more time writing
cypher also allows you to write code that is easier to read

USE CASES OF GRAPH DATABASES
real time recommendations
master data management
fraud detection
graph based search
metwork-it operations
identify&access management

LABELS ARE KIND OF LIKE TYPES

MAPPING TO LANGUAGES
NODES are NOUNS in our language
PROPERTIES are ADJECTIVES that describe those nouns
RELATIONSHIPS are VERBS
PROPERTIES ON THE RELATIONSHIPS are our ADVERBS

cypher
CREATE(:Person{name:"Ann"}) - [:LOVES] -> (:Person{name:"Ann"})

:Person{name:"Ann"} -> NODE
:Person -> LABEL
{name:"Ann"} -> PROPERTY / json key, value pair

CREATE(:LABEL{PROPERTY})

3 steps to using neo4j
1. creating our model
create your labels, your nodes, properties, relationships,
2. loading your data
CSV file
3. querying

SQL PAINS
complex to model and store relationships
performance degrades with incereases in data
queries get long and complex
maintenance is painful

https://www.youtube.com/watch?v=NO3C-CWykkY&list=PL9Hl4pk2FsvWM9GWaguRhlCQ-pa-ERd4U&index=6
converting your model
1. first locate the foreign keys and we eliminate the foreign keys and we replace those with relationships
2. locate at our join tables.
join tables becomes RELATIONSHIPS
attributed join tables -> RELATIONSHIPS WITH PROPERTIES

graph gains
easy to model and store relationships
performance of relationship traversal remains constant with growing in data size
queries are shortened and more readable
adding additional properties and relationships can ben done fly - no migrations

AsciiArt for Nodes
 nodes are surrounded by parenthesis () or (p)
 Labels, or tags, start with : and group nodes by roles or types (p:Person:Mammal)
 Nodes can have properties (p:Person{name:'Veronica'})

AsciiArt for Relationship
Relationships are wrapped with dash/hyphens or square brackets --> or -[h:HIRED]->
Direction of relationship is specified with < >
(p1)-[:HIRED]->(p2) or (p1)<-[:HIRED]-(p2)
Relationships have properties too
-[:HIRED {type:'fulltime'})->



