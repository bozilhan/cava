package com.pogrebinsky.s002threadcreation;

public class S002CreateThread {
    public static void main(String[] args) throws InterruptedException {
        /*
         * all the threads related properties and methods are encapsulated in Thread class
         *
         *
         * we can define our own Runnable and then pass it to Thread constructor
         * AS a ANONYMOUS object
            public class Task2 implements Runnable {
                @Override
                public void run() { System.out.println("Hello from new thread");}
            }

            Thread thread = new Thread(new Task2());
            thread.start();

         * 1. create new thread with lambda or override run() method of Runnable interface
         * 2. .setName() give meaningful name the created thread FOR DEBUGGING
         * 3. .setUncaughtExceptionHandler((Thread t, Throwable e) -> {})
         * 4. .start() -> EXECUTE the thread
         * */
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                /*
                 * the code that we run inside this run() method
                 * is going to be run on that NEW THREAD
                 * as soon as thread is scheduled by the OS
                 * */
            }
        });

        Thread threadWithLambda = new Thread(() -> {
            /*
             * the code that we run inside this run() method
             * is going to be run on that NEW THREAD
             * as soon as thread is scheduled by the OS
             * */
            System.out.println("We are now in thread: " + Thread.currentThread().getName());
            System.out.println("Current thread priority is " + Thread.currentThread().getPriority());
        });

        /*
         * JVM gave our new thread a pretty UNHELPFUL NAME `Thread-0`
         * But when we start creating applications WITH LOTS OF THREADS AND MEANINGFUL NAME for each thread
         *
         * Giving MEANINGFUL name will HELP us a lot in DEBUGGING
         *
         * ALWAYS give a MEANINGFUL name while creating thread
         * */
        threadWithLambda.setName("New worker thread");

        /*
         * print which thread we're currently executing from
         *
         * Thread.currentThread() gives us the thread object of THE CURRENT THREAD
         *
         * Thread.currentThread().getName() OR
         * Thread.currentThread().getId()
         * */
        System.out.println("We are in thread: " + Thread.currentThread().getName() + " before starting a new thread");

        /*
         * Just creating the threat object is NOT ENOUGH.
         * We need to, to actually start the thread by calling the
         * start() method on the thread object.
         *
         * This will instruct the JVM to create a new thread and pass it to the OS.
         * */
        threadWithLambda.start();
        /*
         * AT THIS POINT we will have the threadWithLambda and the main thread
         * running CONCURRENTLY
         * */

        /*
         * we can pass value to 1 to 10 or we can use PREDEFINED VALUES
         *
         * Dynamic Priority = Static Priority + Bonus
         * Dynamic Priority = setPriority() + Bonus
         *
         * in more complex programs WHERE SOME THREADS NEED MORE RESPONSIVENESS THAN THE OTHERS,
         * This will play an important role.
         *  */
        threadWithLambda.setPriority(Thread.MAX_PRIORITY);

        /*
         * NOTICE THAT!!!
         * after we called the thread start method,
         * THE NEW THREAD HASN'T BEEN SCHEDULED YET.
         * Cause that takes some time. And it happens a synchronously by the OS.
         * SO THE SECOND LINE WE SEE IS ALSO FROM THE MAIN THREAD.
         * */
        System.out.println("We are in thread: " + Thread.currentThread().getName() + " after starting a new thread");

        /*
         * sleep() method puts THE CURRENT THREAD to sleep for a given number of MILLISECONDS
         * sleep method, instructs the operating system
         * to NOT SCHEDULE the CURRENT THREAD UNTIL THAT TIME PASSES.
         * During that time, this thread is NOT CONSUMING any CPU.
         *
         * to determine which thread is current thread
         * Thread.currentThread().getName() OR
         * Thread.currentThread().getId()
         * */
        Thread.sleep(10000);
    }
}
