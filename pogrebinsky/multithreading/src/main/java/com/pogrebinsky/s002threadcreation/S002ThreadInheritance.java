package com.pogrebinsky.s002threadcreation;

public class S002ThreadInheritance {
    public static void main(String[] args) {
        /*
         * instead of creating a generic thread object
         * we create a CONCRETE NewInheritedThread object
         * */
        Thread myNewInheritedThread = new NewInheritedThread();

        myNewInheritedThread.start();
    }

    /*
     * by using this approach
     * We get access to a lot of data and methods that
     * DIRECTLY related to the current thread.
     * */
    private static class NewInheritedThread extends Thread {
        /*
         * All we need to do override run() method
         *
         * instead of calling static method Thread.currentThread() to get the current thread
         * we can use `this` keyword when we define a new class
         * */
        @Override
        public void run() {
            System.out.println("Hello from " + this.getName());
        }
    }
}
