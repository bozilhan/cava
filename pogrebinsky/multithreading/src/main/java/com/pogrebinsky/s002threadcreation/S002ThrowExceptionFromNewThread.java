package com.pogrebinsky.s002threadcreation;

public class S002ThrowExceptionFromNewThread {
    public static void main(String[] args) {

        /*
         * Normally unchecked exceptions that happen in Java, simply BRING DOWN(indirmek) the ENTIRE THREAD
         * if we dont catch unchecked exceptions explicitly and handle unchecked exceptions in a particular way.
         * */
        Thread threadWithLambda = new Thread(() -> {
            throw new RuntimeException("Intentional Exception inside threadWithLambda");
        });

        threadWithLambda.setName("threadWithLambda");

        /*
         * We can set an exception handler for the entire thread at its inception(baslangic).
         *
         * That handler will be called if an exception was thrown INSIDE the threadWithLambda
         * the exception DID NOT GET CAUGHT ANYWHERE IN THIS CASE!!!
         * */
        threadWithLambda.setUncaughtExceptionHandler((Thread t, Throwable e) -> {
            /*
             * would be a place where
             * we could CLEAN UP SOME OF THE RESOURCES or
             * LOG ADDITIONAL DATA TO ENABLE US TROUBLESHOOT THIS ISSUE.
             * */
            System.out.println("A critical error happened in thread: " + t.getName() + " the error is " + e.getMessage());
        });

        threadWithLambda.start();
    }
}
