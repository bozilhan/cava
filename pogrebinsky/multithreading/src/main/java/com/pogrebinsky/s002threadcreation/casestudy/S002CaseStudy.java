package com.pogrebinsky.s002threadcreation.casestudy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/*
 * I want to see how long it would take the hackers to break into the vault.
 * By guessing my code, WE WILL HAVE A FEW HACKER THREADS TRYING TO BRUTE FORCE MY CODE CONCURRENTLY.
 *
 * In addition to that, we're going to HAVE A POLICE THREAD.
 * That thread is going to come to our rescue by counting down 10 seconds.
 * And if the hackers haven't broken into the vault by then and ran away with the money, the policemen is going to
 * arrest them
 *
 * while counting down the police thread is going to show us the progress of its arrival.
 * */
public class S002CaseStudy {
    public static final int MAX_PASSWORD = 9999;

    public static void main(String[] args) {
        /*
         * generate random password on each run
         * */
        Random randomPassword = new Random();

        Vault vault = new Vault(randomPassword.nextInt(MAX_PASSWORD));

        /*
         * Because they all inherit from thread.
         * We can treat them all as threads, REGARDLESS(ne olursa olsun) of their concrete type,
         * which is also called POLYMORPHISM in object oriented programming.
         * */
        List<Thread> threads = Arrays.asList(
                new AscendingHackerThread(vault),
                new DescendingHackerThread(vault),
                new PoliceThread()
        );

        /*
         * call the start() method on each thread
         * */
        for (Thread thread : threads) {
            thread.start();
        }
    }

    private static class Vault {
        private final int password;

        public Vault(int password) {
            this.password = password;
        }

        public boolean isCorrectPassword(int guess) {
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return guess == password;
        }
    }

    /*
     * generic hacker thread
     * */
    private static abstract class HackerThread extends Thread {
        protected final Vault vault;

        HackerThread(Vault vault) {
            this.vault = vault;

            /*
             * set each concrete hacker thread's name as the class name,
             * */
            this.setName(this.getClass().getSimpleName());

            this.setPriority(Thread.MAX_PRIORITY);
        }

        /*
         * this class extends Thread class
         * we can OVERRIDE any of Thread class' methods
         * */
        @Override
        public void start() {
            System.out.println("Starting thread: " + this.getName());
            super.start();
        }
    }

    /*
     * This thread is simply going to guess our password
     * by iterating all the numbers in an ascending order
     * */
    private static class AscendingHackerThread extends HackerThread {
        public AscendingHackerThread(Vault vault) {
            super(vault);
        }

        @Override
        public void run() {
            for (int guess = 0; guess < MAX_PASSWORD; ++guess) {
                if (vault.isCorrectPassword(guess)) {
                    System.out.println(this.getName() + " guessed the password " + guess);

                    /*
                     * stop the program
                     * */
                    System.exit(0);
                }
            }
        }
    }

    private static class DescendingHackerThread extends HackerThread {
        public DescendingHackerThread(Vault vault) {
            super(vault);
        }

        @Override
        public void run() {
            for (int guess = MAX_PASSWORD; guess > 0; --guess) {
                if (vault.isCorrectPassword(guess)) {
                    System.out.println(this.getName() + " guessed the password " + guess);

                    /*
                     * stop the program
                     * */
                    System.exit(0);
                }
            }
        }
    }

    /*
     * PoliceThread extends thread directly.
     * So PoliceThread doesn't get all the functionality encapsulated in the hacker thread.
     * */
    private static class PoliceThread extends Thread {
        /*
         * It's going to count down from 10 to zero sleeping for one second in between each count.
         * */
        @Override
        public void run() {
            for (int i = 10; i >= 0; --i) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println(i);
            }

            System.out.println("Game over for you hackers");

            System.exit(0);
        }
    }
}
