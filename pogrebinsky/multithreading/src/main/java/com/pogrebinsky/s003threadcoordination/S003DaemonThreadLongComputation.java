package com.pogrebinsky.s003threadcoordination;

import java.math.BigInteger;

public class S003DaemonThreadLongComputation {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new LongComputationTask(new BigInteger("20000"), new BigInteger("1000000")));

        /*
        * even though the long calculation has not finished,
        * just the fact that the main thread ended makes the entire app terminate
        * */
        thread.setDaemon(true);

        thread.start();

        Thread.sleep(100);

        /*
         * However, in this case, just calling the thread, that interrupt is not enough
         * because we can see the interrupt is sent, but we do not have any method or logic to handle it.
         * So that long computation thread is still running.
         * */
        thread.interrupt();
    }

    private static class LongComputationTask implements Runnable {
        private final BigInteger base;
        private final BigInteger power;

        public LongComputationTask(BigInteger base, BigInteger power) {
            this.base = base;
            this.power = power;
        }

        @Override
        public void run() {
            System.out.println(base + "^" + power + " = " + pow(base, power));
        }

        private BigInteger pow(BigInteger base, BigInteger power) {
            BigInteger result = BigInteger.ONE;

            for (BigInteger i = BigInteger.ZERO; i.compareTo(power) != 0; i = i.add(BigInteger.ONE)) {
                result = result.multiply(base);
            }

            return result;
        }
    }
}
