package com.pogrebinsky.s003threadcoordination;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class S003ThreadJoin {
    public static void main(String[] args) throws InterruptedException {
        /*
         * We want to calculate 0!, 3435!, 35435!, 2324!, 4656!, 23!, 2435!, 5566!
         * Factorial calculation is CPU intensive(yogun) task
         * which involves a lot of multiplications
         *
         * we want to calculate each numbers' factorial to a DIFFERENT THREAD
         * this way we can calculate all numbers IN PARALLEL
         * */
        List<Long> inputNumbers = Arrays.asList(0L, 3435L, 35435L, 2324L, 4656L, 23L, 2435L, 5566L);

        /*
         * we want to capture all the results from all the FactorialThread and print them
         * */
        List<FactorialThread> threads = new ArrayList<>();

        for (long inputNumber : inputNumbers) {
            threads.add(new FactorialThread(inputNumber));
        }

        for (Thread thread : threads) {
            /*
            * RACE CONDITION
            * between this line of code where the factorial threads are started
            * */
            thread.start();
        }

        /*
        * thread.join() method in between those lines of code, where the race is happening
        *
        * to resolve that race by FORCING the MAIN THREAD TO WAIT UNTIL
        * ALL the FactorialThreads are FINISHED
        * */
        for (Thread thread : threads) {
            /*
            * For every thread, the thread joined method will return only
            * when that thread has terminated.
            *
            * And by the time the main thread finishes this loop,
            * all the factorial threads are guaranteed to have finished.
            *
            * we want to tolerate more than 2 seconds a thread hasnt terminated
            * the join() method would RETURN
            * */
            thread.join(2000);
        }

        for (int i = 0; i < inputNumbers.size(); ++i) {
            FactorialThread factorialThread = threads.get(i);

            /*
            * RACE CONDITION
            * And this line of code with the, where the main thread is checking for the results.
            *
            * In other words, the factorial threads and the main thread are racing
            * towards their goals independently.
            * And we don't know which one will be in which stage by the time the main
            * thread is checking for the results.
            * */
            if (factorialThread.isFinished()) {
                System.out.println("Factorial of " + inputNumbers.get(i) + " is " +
                                         factorialThread.getResult());
            } else {
                System.out.println("The calculation for " + inputNumbers.get(i) + " is still progress");
            }
        }
    }

    public static class FactorialThread extends Thread {
        private final long inputNumber;

        /*
         * the result may be very big
         * we cannot use long as it may OVERFLOW
         * */
        private BigInteger result = BigInteger.ZERO;
        private boolean isFinished = false;

        public FactorialThread(long inputNumber) {
            this.inputNumber = inputNumber;
        }

        @Override
        public void run() {
            this.result = factorial(inputNumber);
            this.isFinished = true;
        }

        public BigInteger getResult() {
            return result;
        }

        public BigInteger factorial(long n) {
            BigInteger tempResult = BigInteger.ONE;

            for (long i = n; i > 0; --i) {
                tempResult = tempResult.multiply(new BigInteger(Long.toString(i)));
            }
            return tempResult;
        }

        /*
         * coller can find out whether the result is ready or not.
         * */
        public boolean isFinished() {
            return isFinished;
        }

    }
}
