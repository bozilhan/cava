package com.pogrebinsky.s003threadcoordination;

public class S003ThreadTerminationBlockingTask {
    public static void main(String[] args) {
        Thread thread = new Thread(new BlockingTask());

        /*
        * as long as we have AT LEAST ONE THREAD RUNNING,
        * the APPLICATION WILL NOT END.
        * EVEN IF the MAIN thread ALREADY stopped running.
        *
        * if we dont interrupt this thread
        * we CANT EXIT this application!!!
        * */
        thread.start();

        /*
         * to stop THIS blocking thread
         * */
        thread.interrupt();
    }

    private static class BlockingTask implements Runnable {
        @Override
        public void run() {
            // do something

            try {
                Thread.sleep(500000);
            } catch (InterruptedException e) {
                /*
                * that exception is GOING TO BE THROWN
                * WHEN the current thread is INTERRUPTED EXTERNALLY!!!
                *
                * when .interrupt() method is called for instance of BlockingTask
                * */
                System.out.println("exiting blocking thread");
            }
        }
    }
}
