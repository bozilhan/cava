package com.pogrebinsky.s003threadcoordination;

import java.math.BigInteger;

public class S003ThreadTerminationLongComputation {
    public static void main(String[] args) {
        Thread thread = new Thread(new LongComputationTask(new BigInteger("20000"), new BigInteger("1000000")));

        thread.start();

        /*
         * However, in this case, just calling the thread, that interrupt is not enough
         * because we can see the interrupt is sent,
         * but we do not have any method or logic to handle the interrupt for LongComputationTask instances.
         * So that long computation thread is still running.
         * */
        thread.interrupt();
    }

    private static class LongComputationTask implements Runnable {
        private final BigInteger base;
        private final BigInteger power;

        public LongComputationTask(BigInteger base, BigInteger power) {
            this.base = base;
            this.power = power;
        }

        @Override
        public void run() {
            System.out.println(base + "^" + power + " = " + pow(base, power));
        }

        private BigInteger pow(BigInteger base, BigInteger power) {
            BigInteger result = BigInteger.ONE;

            for (BigInteger i = BigInteger.ZERO; i.compareTo(power) != 0; i = i.add(BigInteger.ONE)) {
                /*
                 * in each iteration that checks,
                 * if the current thread got interrupted from the outside world,
                 * the way to do it is to call the isInterrupted() method
                 *
                 * If it the returns true, then we know that we need to
                 * STOP THE CALCULATION AND EXIT FROM THE THREAD.
                 *
                 * if we dont add this control, we dont do anything when interrupt also comes!!!
                 * even if interrupt comes our thread still tries to finish its job
                 * */
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("prematurely interrupted computation");
                    return BigInteger.ZERO;
                }

                result = result.multiply(base);
            }

            return result;
        }
    }
}
