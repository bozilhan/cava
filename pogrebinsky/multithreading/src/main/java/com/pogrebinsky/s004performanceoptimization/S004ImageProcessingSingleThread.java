package com.pogrebinsky.s004performanceoptimization;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class S004ImageProcessingSingleThread {
    public static final String SOURCE_FILE = "s004performanceoptimization/many-flowers.jpg";
    public static final String DESTINATION_FILE = "./out/many-flowers.jpg";

    public static void main(String[] args) throws IOException {
        BufferedImage originalImage = ImageIO.read(new File(SOURCE_FILE));

        BufferedImage resultImage = new BufferedImage(originalImage.getWidth(), originalImage.getHeight(), BufferedImage.TYPE_INT_RGB);

        long startTime = System.currentTimeMillis();
        recolorSingleThreaded(originalImage, resultImage);
        long endTime = System.currentTimeMillis();

        File outputFile = new File(DESTINATION_FILE);
        ImageIO.write(resultImage, "jpg", outputFile);

        long overallDuration = endTime - startTime;

        System.out.println(String.valueOf(overallDuration));
    }

    public static void recolorSingleThreaded(BufferedImage originalImage, BufferedImage resultImage) {
        recolorImage(originalImage, resultImage, 0, 0, originalImage.getWidth(), originalImage.getHeight());
    }

    public static void recolorImage(BufferedImage originalImage, BufferedImage resultImage, int leftCorner, int topCorner, int width, int height) {
        for (int x = leftCorner; x < leftCorner + width && x < originalImage.getWidth(); ++x) {
            for (int y = topCorner; y < topCorner + width && y < originalImage.getWidth(); ++y) {
                recolorPixel(originalImage, resultImage, x, y);
            }
        }
    }

    public static void recolorPixel(BufferedImage originalImage, BufferedImage resultImage, int x, int y) {
        int rgb = originalImage.getRGB(x, y);

        int red = getRed(rgb);
        int green = getGreen(rgb);
        int blue = getBlue(rgb);

        int newRed;
        int newGreen;
        int newBlue;

        if (isShadeOfGray(red, green, blue)) {
            /*
             * not exceed the maximum value that can be stored in a byte (0-255)
             * we take the minimum 255 and the
             * */
            newRed = Math.min(255, red + 10);

            /*
             * to make sure we dont go below 0 we take maximum value
             * */
            newGreen = Math.max(0, green - 80);
            newBlue = Math.max(0, blue - 20);
        } else {
            newRed = red;
            newGreen = green;
            newBlue = blue;
        }

        int newRGB = createRGBFromColors(newRed, newGreen, newBlue);
        setRGB(resultImage, x, y, newRGB);
    }

    public static void setRGB(BufferedImage image, int x, int y, int rgb) {
        image.getRaster().setDataElements(x, y, image.getColorModel().getDataElements(rgb, null));
    }

    /*
     * 0xFF(Alpha) FF(Red) FF(Green) FF(Blue) -> 0xA R G B
     * */

    public static boolean isShadeOfGray(int red, int green, int blue) {
        /*
         * Check all three components have a similar color intensity
         * in other words if particular component is not stronger than the rest
         *
         * 30 is found empirically(deneysel olarak)
         * */
        return Math.abs(red - green) < 30 &&
                Math.abs(red - blue) < 30 &&
                Math.abs(green - blue) < 30;
    }

    public static int createRGBFromColors(int red, int green, int blue) {
        int rgb = 0;
        rgb |= blue;
        rgb |= green << 8;
        rgb |= red << 16;

        /*
         * set Alpha value to the highest value
         * */
        rgb |= 0xFF000000;

        return rgb;
    }

    public static int getRed(int rgb) {
        /*
         * mask Alpha, Green and Blue
         * red component is the third byte from the right
         * shift 2 bytes(16 bits)
         * */
        return (rgb & 0x00FF0000) >> 16;
    }

    public static int getGreen(int rgb) {
        /*
         * mask Alpha, Red and Blue
         * green component is the second byte from the right
         * shift 1 byte(8 bits)
         * */
        return (rgb & 0x0000FF00) >> 8;
    }

    /*
     * Extracts only blue value out of the pixel
     * */
    public static int getBlue(int rgb) {

        /*
         * mask Alpha, Red and Green
         * apply bitmask on the pixel
         * making all the components 0
         * except for the RIGHT MOST BYTE
         * which is blue component
         * */
        return rgb & 0x000000FF;
    }
}
