package com.pogrebinsky.s004performanceoptimization;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class S004ThroughputHTTPServer {
    private static final String INPUT_FILE = "s004PerformanceOptimization/war_and_peace.txt";
    private static final int NUMBER_OF_THREADS = 1;

    public static void main(String[] args) throws IOException {
        String text = new String(Files.readAllBytes(Paths.get(INPUT_FILE)));

        startServer(text);
    }

    private static void startServer(String text) throws IOException {
        /*
         * The second parameter is the backlog size, which defines the size of the queue for the HTTP server request.
         * We're going to keep it at 0. Since all the requests should end up in the thread pool's queue instead.
         * */
        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);

        server.createContext("/search", new WordCountHandler(text));

        Executor executor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

        server.setExecutor(executor);

        /*
         * once the HTTP server start method is called.
         * Our application will start listening to HTTP requests on port 8000
         * every incoming request that belongs to the search route will be handled by WordCountHandler and
         * executed on the one of the threads in the fixed ThreadPool.
         * */
        server.start();
    }


    /*
     * implements the logic of searching and counting the word.
     * */
    private static class WordCountHandler implements HttpHandler {
        private final String text;

        public WordCountHandler(String text) {
            this.text = text;
        }

        @Override
        public void handle(HttpExchange exchange) throws IOException {
            String query = exchange.getRequestURI().getQuery();
            String[] keyValue = query.split("=");
            String action = keyValue[0];
            String word = keyValue[1];

            if (!action.equals("word")) {
                exchange.sendResponseHeaders(400, 0);
                return;
            }

            long count = countWord(word);

            byte[] response = Long.toString(count).getBytes();
            exchange.sendResponseHeaders(200, response.length);

            OutputStream outputStream = exchange.getResponseBody();
            outputStream.write(response);
            outputStream.close();
        }

        private long countWord(String word) {
            long count = 0;
            int index = 0;

            while (index >= 0) {
                index = text.indexOf(word, index);

                if (index >= 0) {
                    count++;
                    index++;
                }
            }

            return count;
        }
    }
}
