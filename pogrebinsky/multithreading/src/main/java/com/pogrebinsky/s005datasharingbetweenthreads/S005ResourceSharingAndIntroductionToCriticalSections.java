package com.pogrebinsky.s005datasharingbetweenthreads;

public class S005ResourceSharingAndIntroductionToCriticalSections {
    public static void main(String[] args) throws InterruptedException {
        /*
        * the core of the problem
        * InventoryCounter is a SHARED OBJECT we pass into BOTH threads
        * which makes the items member variable also SHARED and ACCESSIBLE in both threads
        *
        * items++ and items-- are happening in the SAME TIME
        * those operations are NOT ATOMIC
        *
        * The order of threads' execution depends on the way the oil schedules them.
        * So we may end up with different scenarios every single time.
        * */
        InventoryCounter inventoryCounter = new InventoryCounter();
        IncrementingThread incrementingThread = new IncrementingThread(inventoryCounter);
        DecrementingThread decrementingThread = new DecrementingThread(inventoryCounter);

        incrementingThread.start();
        decrementingThread.start();

        incrementingThread.join();
        decrementingThread.join();

        System.out.println("We currently have " + inventoryCounter.getItems() + " items");
    }

    public static class DecrementingThread extends Thread {
        private final InventoryCounter inventoryCounter;

        public DecrementingThread(InventoryCounter inventoryCounter) {
            this.inventoryCounter = inventoryCounter;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10000; i++) {
                inventoryCounter.decrement();
            }
        }
    }

    public static class IncrementingThread extends Thread {
        private final InventoryCounter inventoryCounter;

        public IncrementingThread(InventoryCounter inventoryCounter) {
            this.inventoryCounter = inventoryCounter;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10000; i++) {
                inventoryCounter.increment();
            }
        }
    }

    private static class InventoryCounter {
        private int items = 0;

        public void increment() {
            items++;
        }

        public void decrement() {
            items--;
        }

        public int getItems() {
            return items;
        }
    }
}
