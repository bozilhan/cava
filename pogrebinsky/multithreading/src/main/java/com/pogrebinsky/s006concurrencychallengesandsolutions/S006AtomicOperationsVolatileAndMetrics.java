package com.pogrebinsky.s006concurrencychallengesandsolutions;

import java.util.Random;

/**
 * Atomic Operations, Volatile & Metrics practical example
 * https://www.udemy.com/java-multithreading-concurrency-performance-optimization
 */
public class S006AtomicOperationsVolatileAndMetrics {
    public static void main(String[] args) {
        Metrics metrics = new Metrics();

        BusinessLogic businessLogicThread1 = new BusinessLogic(metrics);

        BusinessLogic businessLogicThread2 = new BusinessLogic(metrics);

        MetricsPrinter metricsPrinter = new MetricsPrinter(metrics);

        businessLogicThread1.start();
        businessLogicThread2.start();
        metricsPrinter.start();
    }

    /*
    * This MetricsPrinter is going to RUN IN PARALLEL to our BusinessLogic.
    * captured the average time the business logic is taking us and print it to the screen.
    * */
    public static class MetricsPrinter extends Thread {
        private Metrics metrics;

        public MetricsPrinter(Metrics metrics) {
            this.metrics = metrics;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                }

                /*
                * because the getAverage() method is not synchronized.
                * We guarantee that the MetricsPrinter will not slow down the BusinessLogic's
                * threads as MetricsPrinter can be performed 90% in parallel to the other threads.
                * */
                double currentAverage = metrics.getAverage();

                System.out.println("Current Average is " + currentAverage);
            }
        }
    }

    public static class BusinessLogic extends Thread {
        private Metrics metrics;
        private Random random = new Random();

        public BusinessLogic(Metrics metrics) {
            this.metrics = metrics;
        }

        @Override
        public void run() {
            while (true) {
                long start = System.currentTimeMillis();

                try {
                    Thread.sleep(random.nextInt(2));
                } catch (InterruptedException e) {
                }

                long end = System.currentTimeMillis();

                metrics.addSample(end - start);
            }
        }
    }

    /*
     * to capture the symbols
     * to have the average at any given moment
     * */
    public static class Metrics {
        /*
         * average and count are variables that are going to be SHARED by MULTIPLE THREADS
         * because multiple threads shared the Metrics object.
         *
         * to keep count of how many samples we captured so far
         * */
        private long count = 0;

        /*
        * VOLATILE would guarantee is that the write on line 97 is ATOMIC
        * and reading from average in the getter is ALSO ATOMIC.
        * */
        private volatile double average = 0.0;

        /*
         * potentially many threads can add samples to an object of the metrics
         * */
        public synchronized void addSample(long sample) {
            double currentSum = average * count;
            count++;
            average = (currentSum + sample) / count;
        }

        public double getAverage() {
            return average;
        }
    }
}
