package com.pogrebinsky.s006concurrencychallengesandsolutions;

public class S006CriticalSectionsAndSynchronization {
    public static void main(String[] args) throws InterruptedException {
        InventoryCounter inventoryCounter = new InventoryCounter();
        IncrementingThread incrementingThread = new IncrementingThread(inventoryCounter);
        DecrementingThread decrementingThread = new DecrementingThread(inventoryCounter);

        incrementingThread.start();
        decrementingThread.start();

        incrementingThread.join();
        decrementingThread.join();

        System.out.println("We currently have " + inventoryCounter.getItems() + " items");
    }

    public static class DecrementingThread extends Thread {

        private final InventoryCounter inventoryCounter;

        public DecrementingThread(InventoryCounter inventoryCounter) {
            this.inventoryCounter = inventoryCounter;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10000; i++) {
                inventoryCounter.decrement();
            }
        }
    }

    public static class IncrementingThread extends Thread {

        private final InventoryCounter inventoryCounter;

        public IncrementingThread(InventoryCounter inventoryCounter) {
            this.inventoryCounter = inventoryCounter;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10000; i++) {
                inventoryCounter.increment();
            }
        }
    }

    private static class InventoryCounter {
        private int items = 0;

        /*
         * BEGIN APPROACH2
         * */
        Object lock = new Object();

        public void increment() {
            synchronized (this.lock) {
                items++;
            }
        }

        public void decrement() {
            synchronized (this.lock) {
                items--;
            }
        }

        public int getItems() {
            synchronized (this.lock) {
                return items;
            }
        }
        /*
         * END APPROACH2
         * */

        /*
         * BEGIN APPROACH1
         * SYNCHRONIZED MONITOR
         * declare one or more methods in a class using the synchronized keyword
         * if threadA is executing method1,
         * ThreadB is DEPRIVED(yoksun) from execution, BOTH method1 AND method2
         * that's because the SYNCHRONIZED is APPLIED PER OBJECT
         * The term for that is called a MONITOR.
         * */
        public synchronized void incrementSynchronized() {
            items++;
        }

        public synchronized void decrementSynchronized() {
            items--;
        }

        public synchronized int getItemsSynchronized() {
            return items;
        }
        /*
         * END APPROACH1
         * */
    }
}

