package com.pogrebinsky.s006concurrencychallengesandsolutions;

/**
 * Race Conditions & Data Races
 * https://www.udemy.com/java-multithreading-concurrency-performance-optimization
 */
public class S006RaceConditionsAndDataRaces {
    public static void main(String[] args) {
        SharedClass sharedClass = new SharedClass();
        Thread thread1 = new Thread(() -> {
            for (int i = 0; i < Integer.MAX_VALUE; i++) {
                sharedClass.increment();
            }
        });

        Thread thread2 = new Thread(() -> {
            for (int i = 0; i < Integer.MAX_VALUE; i++) {
                sharedClass.checkForDataRace();
            }

        });

        thread1.start();
        thread2.start();
    }

    public static class SharedClass {
        /*
        * to AVOID DATA RACE
        * add VOLATILE keyword
        * */
        private volatile int x = 0;
        private volatile int y = 0;

        public void increment() {
            x++;
            y++;
        }

        /*
        * we're going to just log it and let the threats continue to run
        * */
        public void checkForDataRace() {
            if (y > x) {
                System.out.println("y > x - Data Race is detected");
            }
        }
    }
}
