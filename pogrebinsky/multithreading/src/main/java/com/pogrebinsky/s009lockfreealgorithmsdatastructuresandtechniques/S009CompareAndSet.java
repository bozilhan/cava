package com.pogrebinsky.s009lockfreealgorithmsdatastructuresandtechniques;

import java.util.concurrent.atomic.AtomicReference;

public class S009CompareAndSet {
    public static void main(String[] args) {
        String oldName = "old name";
        String newName = "new name";

        AtomicReference<String> atomicReference = new AtomicReference<>(oldName);

        atomicReference.set("unexpected");

        if (atomicReference.compareAndSet(oldName, newName)) {
            System.out.println("New Value is " + atomicReference.get());
        } else {
            System.out.println("Nothing happened");
        }
    }
}
