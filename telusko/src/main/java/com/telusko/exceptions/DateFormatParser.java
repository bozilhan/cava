package com.telusko.exceptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class DateFormatParser {
    static void convertDateFormat(String inputDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sdf.parse(inputDate);

            SimpleDateFormat outputSdf = new SimpleDateFormat("yyyy-MM-dd");
            String outputDate = outputSdf.format(date);

            System.out.println("After changing date format to yyyy/MM/dd : " + outputDate);

        } catch (ParseException pe) {
            System.out.println("Some error occurred while converting date formats. Exception: " + pe);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter date in dd/MM/yyyy format: ");
        String inputDate = scanner.nextLine();
        convertDateFormat(inputDate);
    }
}
