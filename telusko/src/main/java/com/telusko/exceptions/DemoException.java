package com.telusko.exceptions;

/*
 * Throwable
 *   - Exception
 *       - Checked
 *           - IOException
 *              - FileNotFoundException
 *           - SQLException
 *       - Unchecked
 *           - RuntimeException !!!
 *              - ArithmeticException
 *                   ...
 *                   ...
 *                   ...
 *   - Error
 *
 * if you talk about the exception hierarchy the MAIN class is THROWABLE
 * Generally  XXXX(able) are INTERFACES but Throwable is NOT interface it is a class
 *
 * We can classify statements in 2 types
 * 1. normal statements
 * 2. critical statements
 *
 * Subclass of throwable are Exception and Error
 *
 * Error can NOT be handled
 *   memory is leaking of your JVM
 *   your CPU is not working
 *
 * Exception can be handled. Exception is the super class of the exceptions
 * 1. checked exceptions where we will not face any problem
 *   IOException
 *   SQLException
 *
 * 2. unchecked exceptions in which we may face SOME PROBLEM
 *   RUN TIME EXCEPTIONS
 *   int k = i/j -> the value of j might be 0(ZERO)
 *   any number divided by zero will throw an exception
 *   so that is a CRITICAL STATEMENT
 *   whenever we have critical statements, we always our critical statements INSIDE A BLOCK
 *
 * if that block generates any kind of exception WE HAVE TO HANDLE IT
 * for that we have to use TRY block
 * we have MULTIPLE CATCH with ONE TRY
 *
 *   try {
 *     int i = j/k;
 *     FileOutputStream fos = new FileOutputStream("abc.txt");
 *     // if you find any exception there our POINTER OF EXECUTION WILL GOING TO CATCH BLOCK
 *     // if k = 0 it will THROW the OBJECT of EXCEPTION
 *     // we have to catch the thrown exception
 *     //
 *     // TRY WILL THROW THE EXCEPTION
 *   } catch (ArithmeticException ae){
 *     // CATCH WILL CATCH THE EXCEPTION
 *     // SOME CODE
 *   } catch (IOException ie){
 *     // SOME CODE
 *   } catch (Exception e){
 *     // HANDLES ALL TYPE OF EXCEPTION
 *   }
 *
 * if we dont want to use try/catch block we ADD 'throws Exception' to the DEFINITION of a method!!!
 * for PARTICULAR STATEMENTS we can apply try/catch
 * for the WHOLE METHOD we can apply throws Exception
 *      void foo() throws Exception {
 *       //SOME CODE
 *       // there is no try/catch block
 *      }
 * */
public class DemoException {
    public static void main(String[] args) {
        int i = 8;
        int j = 0;

        /*
         * Whenever we put our assignment IN A BLOCK
         * we make sure our declaration will ASSIGN a VALUE!!!
         *
         * because there are chances that this statement
         * might NOT be executed
         * */
        int k = 0;

        try {
            /*
             * if the statement throws an error
             * we HAVE TO CATCH the error
             *
             * if you find any exception there our POINTER OF EXECUTION WILL GOING TO CATCH BLOCK
             * BEFORE PERFORMING ASSIGNMENT ACTION!!!
             *
             * k ye islemin sonucu ATANMADAN catch blockun ILK SATIRINA duser!!!
             * when you get exception, pointer of execution DIRECTLY JUMPS the CATCH BLOCK
             *
             * RUNTIME EXCEPTION
             * UNCHECKED EXCEPTION
             * */
            k = i / j;

            /*
             * statements existing there NEVER BE EXECUTED !!!
             * */
        } catch (Exception e) {
            /*
             * if the exception is IN the MAIN CLASS
             * so we can handle ALL THE EXCEPTION
             * USING Exception class
             * NOT SUBCLASS OF Exception class
             *
             * main class call stack de CIKABILECEGIMIZ SON NOKTA OLDUGUNDAN
             * main class da exception yakalanacaksa ilgili catch tipi
             * Exception class olmali!!!
             * */
            System.out.println("Cannot devide by zero " + e);
        }

        /*
        execution WILL FLOW which will NOT STOP ABRUPTLY(suddenly and unexpectedly)
        catch block tamamlandiktan sonra programin akisi catch blogun bittigi yerden devam eder
        try bloktan once k ya deger atanmazsa -> HATA ALINIR
        variable k might not have been initialized
        */
        System.out.println("k: " + k);
    }
}
