package com.telusko.exceptions;

/*
 * Throwable
 *   - Exception
 *       - Checked
 *           - IOException
 *              - FileNotFoundException
 *           - SQLException
 *       - Unchecked
 *           - RuntimeException !!!
 *              - ArithmeticException
 *                   ...
 *                   ...
 *                   ...
 *   - Error
 *
 * if you talk about the exception hierarchy the MAIN class is THROWABLE
 * Generally  XXXX(able) are INTERFACES but Throwable is NOT interface it is a class
 *
 * We can classify statements in 2 types
 * 1. normal statements
 * 2. critical statements
 *
 * Subclass of throwable are Exception and Error
 *
 * Error can NOT be handled
 *   memory is leaking of your JVM
 *   your CPU is not working
 *
 * Exception can be handled. Exception is the super class of the exceptions
 * 1. checked exceptions where we will not face any problem
 *   IOException
 *   SQLException
 *
 * 2. unchecked exceptions in which we may face SOME PROBLEM
 *   RUN TIME EXCEPTIONS
 *   int k = i/j -> the value of j might be 0(ZERO)
 *   any number divided by zero will throw an exception
 *   so that is a CRITICAL STATEMENT
 *   whenever we have critical statements, we always our critical statements INSIDE A BLOCK
 *
 * if that block generates any kind of exception WE HAVE TO HANDLE IT
 * for that we have to use TRY block
 * we have MULTIPLE CATCH with ONE TRY
 *
 *   try {
 *     int i = j/k;
 *     FileOutputStream fos = new FileOutputStream("abc.txt");
 *     // if you find any exception there our POINTER OF EXECUTION WILL GOING TO CATCH BLOCK
 *     // if k = 0 it will THROW the OBJECT of EXCEPTION
 *     // we have to catch the thrown exception
 *     //
 *     // TRY WILL THROW THE EXCEPTION
 *   } catch (ArithmeticException ae){
 *     // CATCH WILL CATCH THE EXCEPTION
 *     // SOME CODE
 *   } catch (IOException ie){
 *     // SOME CODE
 *   } catch (Exception e){
 *     // HANDLES ALL TYPE OF EXCEPTION
 *   }
 *
 * if we dont want to use try/catch block we ADD 'throws Exception' to the DEFINITION of a method!!!
 * for PARTICULAR STATEMENTS we can apply try/catch
 * for the WHOLE METHOD we can apply throws Exception
 *      void foo() throws Exception {
 *       //SOME CODE
 *       // there is no try/catch block
 *      }
 * */
public class DemoExceptionMultipleCatchUnchecked {
    public static void main(String[] args) {
        int a[] = new int[4];

        /*
         * all these catch block BELONG to this try block!!!
         * */
        try {
            /*
             * when c is 4 pointer of execution jumps
             * first line of ArithmeticException catch block
             * */
            for (int c = 0; c <= 4; c++) {
                a[c] = c + 1;
            }

            for (int value : a) {
                System.out.println(value);
            }

        } catch (ArithmeticException ae) {
            /*
             * The only error we expect is ArithmeticException
             * */
            System.out.println("Cannot devide by zero " + ae);
        } catch (ArrayIndexOutOfBoundsException aioobe) {
            /*
             * The only error we expect is ArrayIndexOutOfBoundsException
             * */
            System.out.println("Maximum number of  values is 4" + aioobe);
        } catch (Exception e) {
            /*
             * it is ALWAYS BETTER to have the LAST catch block AS Exception
             * we HAVE TO write our MAIN EXCEPTION as a LAST CATCH
             * */
            System.out.println("Unknown Exception " + e);
        }
    }
}
