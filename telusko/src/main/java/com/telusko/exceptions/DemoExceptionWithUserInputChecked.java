package com.telusko.exceptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DemoExceptionWithUserInputChecked {
    public static void main(String[] args) throws IOException {
        int j = 0;
        int k = 0;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        /*
         * all these catch block BELONG to this try block!!!
         * */
        try {
            /*
             * there is a PROBABILITY that user enters wrong value, zero etc.
             * */
            j = Integer.parseInt(br.readLine());
            int i = 8;

            /*
             * j 0 oldugunda k ye deger atanmaz print basilmaz
             * ArithmeticException catch bloguna ziplanir
             *
             * once we got error while calculating i / j
             * execution point of the program will directly
             * go to ArithmeticException catch block
             * and program exits
             * */
            k = i / j;
            System.out.println("Output is: " + k);
        } catch (IOException e) {
            /*
             * if we dont write
             * we get 'java.io.IOException' is never thrown in the corresponding try block
             * */
            System.out.println("Some IO error " + e);
        } catch (ArithmeticException e) {
            System.out.println("Cannot devide by zero " + e);
        } catch (Exception e) {
            System.out.println("Unknown Exception " + e);
        } finally {
            /*
             * finally is a block that is used to EXECUTE IMPORTANT CODE such as
             * CLOSING CONNECTION STREAM etc
             *
             * no matter the exception is handled or not finally block will always be executed
             *
             * finally DOESNT MATTER if our code gives an error or not
             * if your try block is executed successfully finally block is called
             * if code throws an error, error calls catch block and at last finally block is called
             *
             * !!! we SHOULD CLOSE ALL OUR RESOURCES in FINALLY BLOCK !!!
             *
             * this block is always executed
             *  */
            br.close();
            System.out.println("BYE");
        }

        System.out.println("k: " + k);
    }
}
