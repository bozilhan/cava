package com.telusko.exceptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DemoThrow {
    public static void main(String[] args) throws IOException {
        int j = 0;
        int k = 0;

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            j = Integer.parseInt(br.readLine());
            int i = 8;
            k = i + j;

            if (k < 10) {
                /*
                 * I want to print an error
                 *
                 * we HAVE TO FORCE throw an error
                 * */

                throw new ArithmeticException();
            }

            System.out.println("Output is: " + k);
        } catch (ArithmeticException e) {
            System.out.println("Minimum value for the output is 10");
        } catch (Exception e) {
            System.out.println("Unknown Exception " + e);
        }

        System.out.println("k: " + k);
    }
}
