package com.telusko.exceptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * try block with resources
 *
 * create a RESOURCE inside TRY BLOCK
 * */
public class InputDemoTryWithResources {
    public static void main(String[] args) throws IOException {


        /*
        * automatic RESOURCE management
        *
        * when you have a try block in this scenario
        * you DONT REQUIRE ANY CATCH BLOCK, FINALLY BLOCK
        *
        * as soon as we are done with our try block this resource will be DEALLOCATED
        * even if we get an error while reading line this resource will be DEALLOCATED
        *
        * we can use it for
        *   SOCKETS
        *   FILES
        *   DATABASE
        *   ANY RESOURCES YOU NEED
        * */
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String str = br.readLine();

        }
    }

}
