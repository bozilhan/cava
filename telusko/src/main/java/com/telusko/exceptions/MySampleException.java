package com.telusko.exceptions;

import java.util.Scanner;

public class MySampleException {
    static void validateInput(int number) throws InvalidateInputException {
        if (number > 100) {
            throw new InvalidateInputException("Exception");
        }

        System.out.println("Valid Input");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number less than 100: ");
        int number = scanner.nextInt();

        try {
            validateInput(number);
        } catch (InvalidateInputException iee) {
            System.out.println("Caught Exception - Input is greater than 100");
        }
    }
}

class InvalidateInputException extends Exception {
    public InvalidateInputException(String message) {
        super(message);
    }
}
