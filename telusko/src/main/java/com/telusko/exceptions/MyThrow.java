package com.telusko.exceptions;

/*
* OUTPUT
* My ArithmeticException caught
 * */

public class MyThrow {
    static void avg() {
        try {
            throw new ArithmeticException("My ArithmeticException");
        } catch (ArithmeticException ae) {
            System.out.println("My ArithmeticException caught");
        }
    }

    public static void main(String[] args) {
        avg();
    }

    /* 
    micro services will
register themselves as soon as they start or come up they will register themselves with this naming
server through an application name or application ID which is a unique ID for each application and
also that URL that is required to communicate with that.
server will automatically be fetch by this name server and it will store all that information 
    */
}
