package com.telusko.exceptions;

/*
* OUTPUT WITH catch
    Main entry
    Inside avg function
    Main - before try block
    Inside avgWithThrow function
    Main inside catch: java.lang.ArithmeticException: MyArithmeticException avgWithThrow
    Main inside finally
    Main exit
    java.lang.ArithmeticException: MyArithmeticException avgWithThrow
	    at com.telusko.exceptions.MyThrows.avgWithThrow(MyThrows.java:10)
	    at com.telusko.exceptions.MyThrows.main(MyThrows.java:21)

* OUTPUT WITH catch
    Main entry
    Inside avg function
    Main - before try block
    Inside avgWithThrow function
    Main inside finally
    Exception in thread "main" java.lang.ArithmeticException: MyArithmeticException avgWithThrow
	    at com.telusko.exceptions.MyThrows.avgWithThrow(MyThrows.java:28)
	    at com.telusko.exceptions.MyThrows.main(MyThrows.java:39)
* */

public class MyThrows {
    static void avg() throws ArithmeticException {
        System.out.println("Inside avg function");
    }

    static void avgWithThrow() throws ArithmeticException {
        System.out.println("Inside avgWithThrow function");
        throw new ArithmeticException("MyArithmeticException avgWithThrow ");
    }

    public static void main(String[] args) {
        System.out.println("Main entry");

        avg();

        System.out.println("Main - before try block");

        try {
            avgWithThrow();
        } catch (ArithmeticException ae) {
            System.out.println("Main inside catch: " + ae);
            ae.printStackTrace();
        }  finally {
            System.out.println("Main inside finally");
        }

        System.out.println("Main exit");
    }
}
