package com.telusko.exceptions;

/*
* OUTPUT
    Before 1st try block
    Number format exception
    Before 2nd try block
    Array Handled
    other statement in parent try block
    BYE
* */
public class NestedTry {
    public static void main(String[] args) {
        try {
            System.out.println("Before 1st try block");
            try {
                int num = Integer.parseInt("barz");
                System.out.println("num: " + num);
            } catch (NumberFormatException nfe) {
                System.out.println("Number format exception");
            }

            System.out.println("Before 2nd try block");
            try {
                int[] a = new int[5];
                a[7] = 9;
                System.out.println("a[7]: " + a[7]);
            } catch (Exception e) {
                System.out.println("Array Handled");
            }

            System.out.println("other statement in parent try block");
        } catch (Exception e) {
            /*
            * we dont declare anything in the MAIN try block
            * therefore the exception is not printed
            *
            * if MAIN try block has any exception the exception is printed
            * */
            System.out.println("handled and recovered");
        }

        System.out.println("BYE");
    }
}
