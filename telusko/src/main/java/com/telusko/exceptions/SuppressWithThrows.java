package com.telusko.exceptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;


/*
* throws -> suppress errors, all exceptions
* I DONT want to HANDLE the ERROR
* but I will SUPPRESS(when you write throws Exception it'll simply allow to run remaining of the lines as it is without using try & catch.) the ERROR
* I know it will throw an error so I will suppress it
*
* if we know we will NOT GET ANY ERROR we can just suppress those errors
* throws IOException, SQLException, ..., ....
*
* throws are always RIGHT AFTER the method
*
* */
public class SuppressWithThrows {
    public static void main(String[] args) throws IOException, SQLException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int j = Integer.parseInt(br.readLine());

    }
}
