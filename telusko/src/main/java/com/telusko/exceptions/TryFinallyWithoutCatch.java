package com.telusko.exceptions;

/*
 * when using finally block the CATCH block is NOT REQUIRED
 * */
public class TryFinallyWithoutCatch {
    public static void main(String[] args) {
        try {
            int num = Integer.parseInt("barz");
            System.out.println("num: " + num);
        } finally {
            System.out.println("finally is always executed");

        }

        System.out.println("other statement");
    }
}
