package com.telusko.exceptions;

/*
 * sometimes buildin exceptions in java are not able to describe a certain situation
 * in such cases a user can also create exceptions and that are called as user-defined exceptions
 *
 * there are two important points
 * 1. user-defined exception MUST EXTEND the Exception class
 * 2. the exception is thrown using a throw keyword
 * */
public class UserDefinedException {
    public static void main(String[] args) {
        // myExceptionFunction();
        myExceptionWithIntegerFunction();
    }

    static void myExceptionFunction() {
        int i = 5;

        try {
            if (i < 10) {
                /*
                 * 2. the exception is thrown using a throw keyword
                 * */
                throw new MyException("errrror");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void myExceptionWithIntegerFunction() {
        try {
            throw new MyExceptionWithInteger(5);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

/*
 * 1. user-defined exception MUST EXTEND the Exception class
 * we can create any exception you want IOException, SQLException
 * */
class MyException extends Exception {
    public MyException(String message) {
        /*
         * when you want to print something with error
         * you should call the constructor of exception
         * */
        super(message);
    }
}

class MyExceptionWithInteger extends Exception {
    int a;

    public MyExceptionWithInteger(int a) {
        this.a = a;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Exception Number = " + a;
    }
}
