package com.telusko.patterns;

public class Patterns {
    public Patterns() {
    }

    /*
     * * * * *
     * * * * *
     * * * * *
     * * * * *
     * */
    public void printBox(int size) {
        // i acts like a number of ROWS
        // outer loop is responsible for COUNTING NUMBER OF ROWS
        for (int row = 1; row <= size; ++row) {

            // j acts like a number of COLUMNS
            // inner loop is responsible for COUNTING NUMBER OF COLUMNS
            for (int col = 1; col <= size; ++col) {
                // VERTICAL print
                System.out.print("* ");
            }

            // HORIZONTAL print
            System.out.println();
        }
    }

    /*
     *
     * *
     * * *
     * * * *
     * */
    public void printTriangle(int size) {
        // i acts like a number of ROWS
        for (int row = 1; row <= size; ++row) {

            // j acts like a number of COLUMNS
            // number of columns changes depend on ROW -> j <= i
            for (int col = 1; col <= row; ++col) {
                // VERTICAL print
                System.out.print("* ");
            }

            // HORIZONTAL print
            System.out.println();
        }
    }

    /*
     * * * * *
     *       *
     *       *
     * * * * *
     * */
    public void printRectangle(int size) {
        for (int row = 1; row <= size; ++row) {
            for (int col = 1; col <= size; ++col) {
                boolean isStar = (row == 1 || row == size || col == 1 || col == size);

                if (isStar) {
                    System.out.print("*");
                } else {
                    // print space
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    /*
     * 1 2 3 4
     * 2 3 4 1
     * 3 4 1 2
     * 4 1 2 3
     * */
    public void printNumbers(int size) {
        for (int row = 1; row <= size; ++row) {
            for (int col = 1; col <= size; ++col) {
                System.out.print(row);
                row++;
                if (row >= size + 1) {
                    row = 1;
                }

            }
            System.out.println();
        }
    }

    /*
     * 1 1 1 1
     * 2 2 2 2
     * 3 3 3 3
     * 4 4 4 4
     * */
    public void printRowNumber(int size) {
        for (int row = 1; row <= size; ++row) {
            for (int col = 1; col <= size; ++col) {
                // print i
                System.out.print(row + " ");
            }
            System.out.println();
        }
    }

    /*
     * 1 2 3 4
     * 1 2 3 4
     * 1 2 3 4
     * 1 2 3 4
     * */
    public void printColumnNumber(int size) {
        for (int row = 1; row <= size; ++row) {
            for (int col = 1; col <= size; ++col) {
                // print j
                System.out.print(col + " ");
            }
            System.out.println();
        }
    }

    /*
     * 1
     * 0 1
     * 1 0 1
     * 0 1 0 1
     * 1 0 1 0 1
     * */
    public void printPattern(int size) {
        for (int row = 1; row <= size; ++row) {
            for (int col = 1; col <= row; ++col) {
                // even
                if (((row + col) & 1) == 0) {
                    System.out.print("1 ");
                } else {
                    System.out.print("0 ");
                }
            }

            System.out.println();
        }
    }

    /*
     * 1
     * 2 2
     * 3 3 3
     * 4 4 4 4
     * 5 5 5 5 5
     * */
    public void printPatternRows(int size) {
        for (int row = 1; row <= size; ++row) {
            for (int col = 1; col <= row; ++col) {
                System.out.print(row + " ");
            }
            System.out.println();
        }
    }
}
