package com.telusko.patterns;

import org.junit.jupiter.api.Test;

class PatternsTest {
    private final Patterns patterns = new Patterns();
    private final int size = 4;

    @Test
    void testPrintBox() {
        patterns.printBox(size);
    }

    @Test
    void testPrintTriangle() {
        patterns.printTriangle(size);
    }

    @Test
    void testPrintRectangle() {
        patterns.printRectangle(size);
    }

    @Test
    void testPrintNumbers() {
        patterns.printNumbers(size);
    }

    @Test
    void printPattern() {
        patterns.printPattern(5);
    }

    @Test
    void printPatternRows() {
        patterns.printPatternRows(5);
    }
}